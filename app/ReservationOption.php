<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationOption extends Model
{
    protected $table = 'reservation_options';
    protected $fillable = ['name'];
}
