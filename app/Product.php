<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table='MATERIAL_MASTER';
    protected $connection ='sqlsrv2';
    protected $fillable = [
        'MATNR', 'EAN11', 'MAKTX'
    ];
//    protected $fillable = [
//        'category_id', 'product_name', 'product_code','root','buying_price','selling_price','supplier_id','buying_date','image','product_quantity'
//    ];

}
