<?php

namespace App\Imports;

use App\Model\MarketingInventory;

//use Illuminate\Contracts\Validation\Validator;

use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Validation\Rule;
use DB;
use DateTime;
use Spatie\Activitylog\Models\Activity;

class ExcelImport implements ToCollection
{
    /**
     * @param Collection $collection
     */
//    private $collection = 0;
    public function collection(Collection $collection)
    {
//        dd($collection);

//        Validator::make($collection->toArray(), [
//
//            '0' => 'exists:marketing_inventory,marketing_MATNR',
//            '4' => 'exists:marketing_inventory,marketing_plantbranch_WERKS',
//        ])->validate();

        $host = gethostname();
        $name = auth()->user()->name;
        foreach ($collection as $key => $value) {
            if ($key > 0) {
                $dateExcelFrom = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value[4])->format('d/m/Y');
                $dateExcelTo = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value[5])->format('d/m/Y');
                $query = MarketingInventory::updateOrCreate(['marketing_MATNR' => $value[0], 'marketing_plantbranch_WERKS' => $value[3]],
                    ['marketing_total_amount' => $value[2], 'marketing_MAKTX' => $value[1], 'valid_from' => $dateExcelFrom,
                        'valid_to' => $dateExcelTo,
                        'market_date' => date('d/m/Y'), 'market_month' => date('F'), 'market_year' => date('Y')]);
                activity('Marketing Bulk - Create')
                    ->log('[Excel]' . ' ' . $name . ' ' . 'Has Created' . ' ' . $value[1] . ' ' . 'Promo' . ' ' . 'To' . ' ' . $value[3]
                        . ' ' . 'Using' . ' ' . $host . ' ' . 'Computer');
                $activity = Activity::all()->last();
            }
        }

        //https://github.com/Maatwebsite/Laravel-Excel/issues/2388
//        https://stackoverflow.com/questions/56985320/maatwebsite-excel-3-1-how-do-i-skip-duplicate-data-when-imported
//        ++$this->collection;
//        foreach($collection as $key=>$value){
//dd($collection);
//
//            $query = MarketingInventory::updateOrCreate(['marketing_MATNR' => $value[0],'marketing_plantbranch_WERKS' => $value[4]],
//                ['marketing_inv' => $value[2],'marketing_total_amount' => $value[3], 'marketing_MAKTX' => $value[1],
//                    'cost' => $value[5], 'market_date' => date('d/m/Y'),'market_month' => date('F'),'market_year' => date('Y')]);
//            dd($key,$value[2]);
//            if($key>0){
//                dd('test');
//                            dd($key,$value);
//                DB::table('marketing_inventory')->insert(['marketing_MATNR' => $value[0],'marketing_MAKTX' => $value[1],
//                   'marketing_inv' => $value[2],'marketing_total_amount' => $value[3], 'marketing_plantbranch_WERKS' => $value[4],
//                    'cost' => $value[5]]);
//                $date1 = date('d/m/Y');
//                $date1 = $value[6];
//                dd(MarketingInventory::where('marketing_MATNR', '=', $value[0])
//                    ->where('marketing_plantbranch_WERKS', '=', $value[4])->get());

//                dd(\Illuminate\Support\Facades\DB::table('marketing_inventory')
//                    ->where('marketing_MATNR', '=', $value[0])
//                        ->where('marketing_plantbranch_WERKS', '=', $value[4])
//                        ->first());
//                dd($key,$value[4],$value[0]);
//                if (\Illuminate\Support\Facades\DB::table('marketing_inventory')
//                    ->where('marketing_MATNR', '=', $value[0])
//                    ->where('marketing_plantbranch_WERKS', '=', $value[4])
//                    ->first() !== null) {
//
//
//                }else{

//                    dd('test123');
//                    $query = MarketingInventory::create(['marketing_MATNR' => $value[0],'marketing_MAKTX' => $value[1],
//                        'marketing_inv' => $value[2],'marketing_total_amount' => $value[3], 'marketing_plantbranch_WERKS' => $value[4],
//                        'cost' => $value[5], 'market_date' => date('d/m/Y'),'market_month' => date('F'),'market_year' => date('Y')]);

//                    $query = MarketingInventory::updateOrCreate(['marketing_MATNR' => $request->marketing_material, 'marketing_plantbranch_WERKS' => $p],
//                        ['marketing_MAKTX' => $request->marketing_description, 'marketing_inv' => $request->marketing_inventory,
//                            'cost' => $request->cost, 'marketing_total_amount' => $request->marketing_total_amount]);

//dd('test123');

//                    $query = MarketingInventory::updateOrCreate(['marketing_MATNR' => $value[0],'marketing_plantbranch_WERKS' => $value[4]],
//                        ['marketing_inv' => $value[2],'marketing_total_amount' => $value[3], 'marketing_MAKTX' => $value[1],
//                        'cost' => $value[5], 'market_date' => date('d/m/Y'),'market_month' => date('F'),'market_year' => date('Y')]);
//                }

//dd($query);
//                $query = Hold::create($odata);

//                $marketing = new MarketingInventory();
//       $marketing->marketing_MATNR = $row[0];
//       $marketing->marketing_MAKTX = $row[1];
//       $marketing->marketing_inv = $row[2];
//       $marketing->marketing_total_amount = $row[3];
//       $marketing->marketing_plantbranch_WERKS = $row[4];
//       $marketing->cost = $row[5];
//       DB
//            }

//        }

    }

}
