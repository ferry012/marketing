<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReasonType extends Model
{
    protected $table = 'reason_types';
    protected $fillable = ['name'];
}
