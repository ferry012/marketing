<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationHeader extends Model
{
    protected $connection = 'sqlsrv';
    protected $table = 'reservation_header';
    protected $fillable = ['res_no', 'customer_id', 'cashier_id', 'salesman_id', 'payment_id', 'total', 'status', 'overall_discount'];

    protected $casts = [
        'created_date' => 'datetime:Y-m-d',
        'updated_at'=>'datetime:m/d/Y h:i s'
    ];

}
