<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salesman extends Model
{
    protected $table='PA0001';
    protected $connection ='sqlsrv2';
    protected $fillable = [
        'PERNR', 'WERKS','BUKRS', 'SNAME'
    ];
}
