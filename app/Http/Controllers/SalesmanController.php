<?php

namespace App\Http\Controllers;

use App\Salesman;
use Illuminate\Http\Request;

class SalesmanController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function search(Request $request)
    {
        $searchCode = $request->get('searchCode');
        $query = Salesman::where('SNAME','LIKE',"%$searchCode%")
            ->orwhere('PERNR','LIKE',"%$searchCode%")
            ->orderBy('SNAME','asc')->get();
        return response()->json($query);

    }
}
