<?php

namespace App\Http\Controllers;

use App\Http\Resources\ReasonCollection;
use App\Http\Resources\ReasonResource;
use App\ReasonType;
use \Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;

class ReasonTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return ReasonCollection
     */
    public function index(Request $request)
    {
        if(!Gate::allows('permission_view' || 'cashier_view' )){
            return abort(403);
        }
        if($request->showAll){
            $query = ReasonType::orderBy('name','asc')->get();
        }else{
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderDir = $request->sortdir;
            $perpage = $request->currentpage;

            $query = ReasonType::where('name','LIKE',"%$searchValue%")
                    ->orderBy($orderBy,$orderDir)->paginate($perpage);
        }
        return new ReasonCollection($query);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        if (! Gate::allows('reason_create')) {
            return abort(403);
        }
        $request->validate([
            'name'=>'required|unique:reason_types,name'
        ]);

        $reason = ReasonType::create($request->all());
        if($reason){
            return response()->json([
                'status'=>'success',
                'message'=>'Reason Type Successfully Added'
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return ReasonResource
     */
    public function show($id)
    {
        if (! Gate::allows('reason_view')){
            return abort(403);
        }
        return new ReasonResource(ReasonType::find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('reason_edit')){
            return abort(403);
        }

        return new ReasonResource(ReasonType::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request,$id)
    {
        if (! Gate::allows('reason_edit')){
            return abort(403);
        }

        $request->validate([
           'name'   =>  'required|unique:reason_types,name'
        ]);

        $reason = ReasonType::find($id);
        $reason->name = $request->name;
        $reason->save();

        if($reason){
            return response()->json([
                'status'=>'success',
                'message'=>'Reason Type Successfully Update'
            ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        if(! Gate::allows('reason_delete')){
            return abort(403);
        }

        $reason = ReasonType::findOrFail($id);
        $reason->delete();

        if($reason){
            return response()->json([
                'status' => 'success',
                'message' => 'Reason Type successfully deleted'
            ]);
        }
    }
}
