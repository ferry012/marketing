<?php

namespace App\Http\Controllers;

use App\Http\Resources\ReservationOptionCollection;
use App\Http\Resources\ReservationOptionResource;
use App\ReservationOption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ReservationOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        // if(!Gate::allows('reservation_option_view')){
        //     return abort(403);
        // }
        if($request->showAll){
            $query = ReservationOption::orderBy('name','asc')->get();
        }else{
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderDir = $request->sortdir;
            $perpage = $request->currentpage;

            $query = ReservationOption::where('name','LIKE',"%$searchValue%")
                ->orderBy($orderBy,$orderDir)->paginate($perpage);
        }
        return new ReservationOptionCollection($query);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (! Gate::allows('reservation_option_create')) {
            return abort(403);
        }
        $request->validate([
            'name'=>'required|unique:reason_types,name'
        ]);

        $reason = ReservationOption::create($request->all());
        if($reason){
            return response()->json([
                'status'=>'success',
                'message'=>'Reservation Option Successfully Added'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return ReservationOption::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (! Gate::allows('reservation_option_edit')){
            return abort(403);
        }

        return new ReservationOptionResource(ReservationOption::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        if (! Gate::allows('reservation_option_edit')){
            return abort(403);
        }

        $request->validate([
            'name'   =>  'required|unique:reason_types,name'
        ]);

        $reason = ReservationOption::find($id);
        $reason->name = $request->name;
        $reason->save();

        if($reason){
            return response()->json([
                'status'=>'success',
                'message'=>'Reservation Option Successfully Update'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(! Gate::allows('reservation_option_delete')){
            return abort(403);
        }

        $reason = ReservationOption::findOrFail($id);
        $reason->delete();

        if($reason){
            return response()->json([
                'status' => 'success',
                'message' => 'Reservation Option successfully deleted'
            ]);
        }
    }
}
