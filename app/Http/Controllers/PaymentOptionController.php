<?php

namespace App\Http\Controllers;

use App\Http\Resources\PaymentCollection;
use App\Http\Resources\PaymentResource;
use App\PaymentOption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PaymentOptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return PaymentCollection
     */
    public function index(Request $request)
    {
        if(! Gate::allows('payment_view')){
            return abort(403);
        }

        if($request->showAll){
            $query = PaymentOption::orderBy('name','asc')->get();
        }else{
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderDir = $request->sortdir;
            $perpage = $request->currentpage;

            $query = PaymentOption::where('name','LIKE',"%$searchValue%")
                    ->orderBy($orderBy,$orderDir)->paginate($perpage);
        }
        return new PaymentCollection($query);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if (! Gate::allows('payment_create')) {
            return abort(403);
        }

        $request->validate([
            'name' => 'required|unique:payment_options,name',
        ]);

        $payment = PaymentOption::create($request->all());
        if($payment){
            return  response()->json([
                'status'=>'success',
                'message'=>'Payment Successfully Added'
            ]);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return PaymentResource
     */
    public function edit($id)
    {
        if (! Gate::allows('payment_edit')) {
            return abort(403);
        }
        return new PaymentResource(PaymentOption::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        if(! Gate::allows('payment_edit')){
            return abort(403);
        }

        $request->validate([
            'name' => 'required|unique:payment_options,name,'.$request->payment,
        ]);

        $payment = PaymentOption::find($id);
        $payment->name = $request->name;
        $payment->save();

        if($payment){
            return  response()->json([
                'status'=>'success',
                'message'=>'Payment Option Succussfully Update '
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        if(! Gate::allows('payment_delete')){
            return abort(403);
        }

        $paymentOption = PaymentOption::findOrFail($id);
        $paymentOption->delete();

        if($paymentOption){
            return response()->json([
               'status'=>'success',
               'message'=> 'Payment Option Successfully Deleted',
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function search(Request $request)
    {
        $searchCode = $request->get('searchCode');
        $query = PaymentOption::where('name','LIKE',"%$searchCode%")
            ->orderBy('name','asc')->get();
        return response()->json($query);
        dd($query);
    }
}
