<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Storage as StorageAlias;
use Image;
use App\Model\Marketing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MarketingController extends Controller
{
    public function index()
    {

        //  $branch = Marketing::select('pb.NAME1','pb.WERKS')
        //                 ->join('VCYSYS820.dbo.PLANT_BRANCHES as pb','marketing.plantbranch_id','=','pb.WERKS')
        //                 ->orderBy('marketing.id')
        //                 ->get();
        //                 return response()->json($branch);
        // $db = DB::connection('sqlsrv2')->getDatabaseName();
        //  $branch = DB::table('marketing')
        //                     ->join($db.'.dbo.PLANT_BRANCHES as pb','marketing.plantbranch_id','=','pb.WERKS')
        //                     ->select('pb.NAME1','marketing.*')
        //                     ->orderBy('marketing.id')
        //                     ->get();
        //                     return response()->json($branch);

        $db = DB::connection('sqlsrv2')->getDatabaseName();
        $branch = DB::table('marketing')
            ->join($db . '.dbo.PLANT_BRANCHES as pb', 'marketing.plantbranch_id', '=', 'pb.WERKS')
            ->select('pb.NAME1', 'marketing.*')
            ->orderBy('marketing.id')
            ->get();
        return response()->json($branch);

    }

    public function store(Request $request)
    {

        $validateData = $request->validate([
            'plantbranch_id' => ['required'],
            'photo' => ['required'],
        ]);
        $host = gethostname();
        $nameUser = auth()->user()->name;
        $uploadedFiles = $request->photo;
        foreach ($uploadedFiles as $file) {
            $name = hexdec(uniqid()) . '.' . $file->getClientOriginalExtension();
            Image::make($file)->resize(1500, 500)->save('backend/marketing/' . $name);
            $upload_path = 'backend/marketing/' . $name;
            $market = new Marketing;
            $market->plantbranch_id = $request->plantbranch_id;
            $market->photo = $upload_path;
            $market->save();

            activity('Advertisement - Create')
                ->log('[WEB]'.' '.$nameUser.' '.'Has Created Advertisement to'.' '.$request->plantbranch_id.' '.'Using'.' '.$host.' '.'Computer');
        }
//        foreach ($uploadedFiles as $file) {
//            $name = hexdec(uniqid()) . '.' . $file->getClientOriginalExtension();
//            Image::make($file)->resize(1500, 500)->save('backend/marketing/' . $name);
//            $upload_path = 'backend/marketing/' . $name;
//            $market = new Marketing;
//            $market->plantbranch_id = $request->plantbranch_id;
//            $market->photo = $upload_path;
//            $market->save();
//
//        }
        return "Success Upload";



    }


    public function show($id)
    {
        // dd($id);
        $marketing = DB::table('marketing')->where('id', $id)->first();
        // dd($marketing);
        return response()->json($marketing);
    }

    public function update(Request $request, $id)
    {
        $host = gethostname();
        $nameUser = auth()->user()->name;
        $data = array();
        $data['plantbranch_id'] = $request->plantbranch_id;
        $image = $request->newphoto;

        if ($image) {

            $position = strpos($image, ';');
            $sub = substr($image, 0, $position);
            $ext = explode('/', $sub)[1];
            $name = time() . "." . $ext;
            $img = Image::make($image)->resize(1500, 500)->encode('jpg');

            $upload_path = 'backend/marketing/';
//            $upload_path = Storage::put('public/storage/backend/marketing/', $img);
            $image_url = $upload_path . $name;
            $success = $img->save($image_url);

            if ($success) {

                $data['photo'] = $image_url;
                $img = Marketing::where('id', $id)->first();
                $image_path = $img->photo;
                $done = unlink($image_path);
                $user = Marketing::where('id', $id)->update($data);

            }

        } else {

            $oldphoto = $request->photo;
            $data['photo'] = $oldphoto;
            $user = Marketing::where('id', $id)->update($data);
        }
        activity('Advertisement - Update')
            ->log('[WEB]'.' '.$nameUser.' '.'Has Updated Advertisement to'.' '.$request->plantbranch_id.' '.'Using'.' '.$host.' '.'Computer');
    }

    public function destroy($id)
    {
        $host = gethostname();
        $nameUser = auth()->user()->name;
//        $marketing = DB::table('marketing')->where('id', $id)->first();
        $marketing = Marketing::where('id', $id)->first();
        $getPlant = $marketing->plantbranch_id;

        $photo = $marketing->photo;
        if ($photo) {
//            unlink($photo);
            DB::table('marketing')->where('id', $id)->delete();
        } else {
            DB::table('marketing')->where('id', $id)->delete();
        }

        activity('Advertisement - Delete')
            ->log('[WEB]'.' '.$nameUser.' '.'Has Deleted Advertisement to'.' '.$getPlant.' '.'Using'.' '.$host.' '.'Computer');
    }
    public function clickImage(Request $request){
        $click = DB::table('marketing')->where('id', $request->id)->first();
        return response()->json($click);
    }
}
