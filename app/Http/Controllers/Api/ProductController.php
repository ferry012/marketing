<?php

namespace App\Http\Controllers\Api;

use Image;
use App\Product;
use App\InventoryStock;
use App\MaterialMaster;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $materialmaster = MaterialMaster::all(); //assign as categories logic
        return response()->json($materialmaster);


    }

    public function resume(Request $request)
    {
        $showAll = $request->get('showAll');
        $searchCode = $request->get('code');

        if($showAll == '1'){
            $product = DB::table('hold')->where('id',$searchCode)->get();
            // $product = Product::join('A801 as a','a.MATNR','MATERIAL_MASTER.MATNR')
            //     ->join('KONP as k','k.KNUMH','a.KNUMH')
            //     ->where('MAKTX','LIKE',"%$searchCode%")
            //     ->orwhere('EAN11','LIKE',"%$searchCode%")
            //     ->select('k.KBETR','MATERIAL_MASTER.*')
            //     ->orderBy('MAKTX','asc')->get();
        }else{
            $product = DB::table('hold')->where('id',$searchCode)->get();
            // $product = Product::join('A801 as a','a.MATNR','MATERIAL_MASTER.MATNR')
            //     ->join('KONP as k','k.KNUMH','a.KNUMH')
            //     ->where('MAKTX',"$searchCode")
            //     ->orwhere('EAN11',"$searchCode")
            //     ->select('k.KBETR','MATERIAL_MASTER.*')
            //     ->orderBy('MAKTX','asc')->get();
        }

        return response()->json($product);
        // dd($product);
    }

    public function show($id)
    {
        $product = DB::table('products')->where('id',$id)->first();
       return response()->json($product);
    }

 public function allProductHold()
    {
       
        $product = DB::table('hold')->select('hold.*')->get();
        // $product = DB::table('hold')->where('id',$id)->first();
       return response()->json($product);
      
    }
    public function allProductHoldRef($id)
    {
        
        //   $contents = DB::table('pos')->get();
      
        //   $odata = array();
        //   foreach ($contents as $content) {
        //   $odata['pro_id'] = $content->pro_id;
      
        //   DB::table('ref_number')->insert($odata);  
        //   }
        //   // return response()->json($contents);
        //   DB::table('pos')->delete();
        // //   DB::table('ref_number')->delete();
        //   return response('Done'); 
          
      
        // dd($id);
         // $product = DB::table('hold')->get();
    //     $product = DB::table('hold')->join('pos','hold.pos_id','=','pos.id')
    //     ->select('pos.id')->get();
       
    //    return response()->json($product); 
       $product = DB::table('hold')->select('hold.id')->latest();      
       return response()->json($product);
    }





}
