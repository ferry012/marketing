<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MarketingReportCollection;
use App\Model\Invoice;
use Illuminate\Http\Request;
use DateTime;
use DB;
use Carbon\Carbon;

class MarketingReportController extends Controller
{
    public function SearchOrderDate(Request $request)
    {
        $orderdate = $request->date;
        $newdate = new DateTime($orderdate);
        $done = $newdate->format('d/m/Y');
        $order = DB::table('marketing_inventory')
            ->select('marketing_inventory.marketing_MATNR', 'marketing_inventory.marketing_MAKTX', 'marketing_inventory.marketing_inv',
                'marketing_inventory.marketing_total_amount', 'marketing_inventory.marketing_plantbranch_WERKS', 'marketing_inventory.cost')
            ->where('marketing_inventory.market_date', '=', $done)
            ->groupBy('marketing_inventory.marketing_MATNR', 'marketing_inventory.marketing_MAKTX', 'marketing_inventory.marketing_inv',
                'marketing_inventory.marketing_total_amount', 'marketing_inventory.marketing_plantbranch_WERKS', 'marketing_inventory.cost')
            ->get();
        return response()->json($order);

    }

    public function SearchOrderDateInvoice(Request $request)
    {
        $db = DB::connection('sqlsrv2')->getDatabaseName();

        $search = $request->searchTerm;
        $date_from = $request->date_from;
        $newdate_from = new DateTime($date_from);
        $from = $newdate_from->format('Y/m/d');
        $date_to = $request->date_to;
        $newdate_to = new DateTime($date_to);
        $newdate = new DateTime($date_to);
        $to = $newdate_to->format('Y/m/d');

        $month = $newdate->format('m');
        $year = $newdate->format('Y');
        $day = $newdate->format('d');


        if ($from !== $to && $search === null) {
            $order = Invoice::join($db . '.dbo.MBEW as mb', function ($join) {
                $join->on('mb.MATNR', '=', 'invoices.invoice_MATNR');
                $join->on('mb.BWKEY', '=', 'invoices.invoice_plant');
            })
                ->whereBetween('invoices.created_at', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                ->latest()
                ->paginate(5);
            return $order;

//dd($order);
//            return $order = Invoice::join($db . '.dbo.MBEW as mb', function ($join) {
//                $join->on('mb.MATNR', '=', 'invoices.invoice_MATNR');
//                $join->on('mb.BWKEY', '=', 'invoices.invoice_plant');
//            })
//                ->where('invoices.invoice_date', '=', $done)
//                ->paginate(5);

        } else if ($from === $to) {
//            dd('pasok');

            return $order = Invoice::join($db . '.dbo.MBEW as mb', function ($join) {
                $join->on('mb.MATNR', '=', 'invoices.invoice_MATNR');
                $join->on('mb.BWKEY', '=', 'invoices.invoice_plant');
            })
                ->where(DB::raw("MONTH(invoices.created_at)"), $month)
                ->where(DB::raw("DAY(invoices.created_at)"), $day)
                ->where(DB::raw("YEAR(invoices.created_at)"), $year)
                ->latest()
                ->paginate(5);

        } else {
            if ($search) {
//                dd('test123');
                $db = DB::connection('sqlsrv2')->getDatabaseName();
                $users = Invoice::join($db . '.dbo.MBEW as mb', function ($join) {
                    $join->on('mb.MATNR', '=', 'invoices.invoice_MATNR');
                    $join->on('mb.BWKEY', '=', 'invoices.invoice_plant');
                })
//                    ->where('invoices.invoice_date', '=', $done)
                    ->whereBetween('invoices.created_at', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                    ->where(function ($query) use ($search) {
                        $query->where('invoice_plant', 'LIKE', "%$search%")
                            ->orWhere('invoice_date', 'LIKE', "%$search%")
                            ->orWhere('total_amount', 'LIKE', "%$search%")
                            ->orWhere('invoice_market_MAKTX', 'LIKE', "%$search%")
                            ->orWhere('mb.VERPR', 'LIKE', "%$search%")
                            ->orWhere('invoice_number', 'LIKE', "%$search%")
                            ->orWhere('cost_invoice_amount', 'LIKE', "%$search%");
                    })->paginate(100);
            } else {

                $db = DB::connection('sqlsrv2')->getDatabaseName();
                $orderdate = $request->data;
                $newdate = new DateTime($orderdate);
                $done = $newdate->format('d/m/Y');

                $users = Invoice::select('invoices.invoice_plant', 'invoices.invoice_date',
                    'invoices.total_amount', 'mb.VERPR',
                    'invoices.invoice_market_MAKTX',
                    'invoices.cost_invoice_amount',
                    'invoices.invoice_number')
                ->join($db . '.dbo.MBEW as mb', function ($join) {
                    $join->on('mb.MATNR', '=', 'invo.invoice_MATNR');
                    $join->on('mb.BWKEY', '=', 'invo.invoice_plant');
                })
//                    ->where('invo.invoice_date', '=', $done)
                    ->whereBetween('invoices.created_at', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                    ->latest()
                    ->paginate(5);
            }
            return response()->json($users);
        }


    }

    public function InvoiceReport(Request $request)
    {

        $search = $request->searchTerm;
        $date_from = $request->date_from;
        $newdate_from = new DateTime($date_from);
        $from = $newdate_from->format('Y/m/d');
        $date_to = $request->date_to;
        $newdate_to = new DateTime($date_to);
        $newdate = new DateTime($date_to);
        $to = $newdate_to->format('Y/m/d');

        $month = $newdate->format('m');
        $year = $newdate->format('Y');
        $day = $newdate->format('d');

        $orderBy = $request->sortby;
        $orderByDir = $request->sortdir;
        $perPage = $request->currentpage;
        $search = $request->search;

            if($search !== '' && $request->filter === "PlantBranch"){

//                dd($search,'PlantBranch');
                $db = DB::connection('sqlsrv2')->getDatabaseName();
                $users = Invoice::join($db . '.dbo.MBEW as mb', function ($join) {
                    $join->on('mb.MATNR', '=', 'invoices.invoice_MATNR');
                    $join->on('mb.BWKEY', '=', 'invoices.invoice_plant');
                })
                    ->whereBetween('invoices.created_at', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                    ->where(function ($query) use ($search) {
                        $query->where('invoice_plant', 'LIKE', "%$search%");
                    })->orderBy($orderBy, $orderByDir)->paginate($perPage);
//                return new MarketingReportCollection($users);
            }
            else if($search !== '' && $request->filter === "DateofRedemption"){
//                dd($search,'DateofRedemption');
                $db = DB::connection('sqlsrv2')->getDatabaseName();
                $users = Invoice::join($db . '.dbo.MBEW as mb', function ($join) {
                    $join->on('mb.MATNR', '=', 'invoices.invoice_MATNR');
                    $join->on('mb.BWKEY', '=', 'invoices.invoice_plant');
                })
                    ->whereBetween('invoices.created_at', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                    ->where(function ($query) use ($search) {

                        $query->where('invoice_date', 'LIKE', "%$search%");
                    })->orderBy($orderBy, $orderByDir)->paginate($perPage);
//                return new MarketingReportCollection($users);
            }
            else if($search !== '' && $request->filter === "InvoiceNumber"){
//                dd($search,'InvoiceNumber');
                $db = DB::connection('sqlsrv2')->getDatabaseName();
                $users = Invoice::join($db . '.dbo.MBEW as mb', function ($join) {
                    $join->on('mb.MATNR', '=', 'invoices.invoice_MATNR');
                    $join->on('mb.BWKEY', '=', 'invoices.invoice_plant');
                })
                    ->whereBetween('invoices.created_at', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                    ->where(function ($query) use ($search) {

                        $query->where('invoice_number', 'LIKE', "%$search%");
                    })->orderBy($orderBy, $orderByDir)->paginate($perPage);
//                return new MarketingReportCollection($users);
            }
            else if($search !== '' && $request->filter === "InvoiceAmount"){
//                dd($search,'InvoiceAmount');
                $db = DB::connection('sqlsrv2')->getDatabaseName();
                $users = Invoice::join($db . '.dbo.MBEW as mb', function ($join) {
                    $join->on('mb.MATNR', '=', 'invoices.invoice_MATNR');
                    $join->on('mb.BWKEY', '=', 'invoices.invoice_plant');
                })
                    ->whereBetween('invoices.created_at', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                    ->where(function ($query) use ($search) {

                        $query->where('total_amount', 'LIKE', "%$search%");
                    })->orderBy($orderBy, $orderByDir)->paginate($perPage);
//                return new MarketingReportCollection($users);
            }
            else if($search !== '' && $request->filter === "FreebieRedeemed"){
//                dd($search,'FreebieRedeemed');
                $db = DB::connection('sqlsrv2')->getDatabaseName();
                $users = Invoice::join($db . '.dbo.MBEW as mb', function ($join) {
                    $join->on('mb.MATNR', '=', 'invoices.invoice_MATNR');
                    $join->on('mb.BWKEY', '=', 'invoices.invoice_plant');
                })
                    ->whereBetween('invoices.created_at', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                    ->where(function ($query) use ($search) {

                        $query->where('invoice_market_MAKTX', 'LIKE', "%$search%");
                    })->orderBy($orderBy, $orderByDir)->paginate($perPage);
//                return new MarketingReportCollection($users);
            }
            else if($search !== '' && $request->filter === "CostofFreebie"){
//                dd($search,'CostofFreebie');
                $db = DB::connection('sqlsrv2')->getDatabaseName();
                $users = Invoice::join($db . '.dbo.MBEW as mb', function ($join) {
                    $join->on('mb.MATNR', '=', 'invoices.invoice_MATNR');
                    $join->on('mb.BWKEY', '=', 'invoices.invoice_plant');
                })
                    ->whereBetween('invoices.created_at', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                    ->where(function ($query) use ($search) {

                        $query->where('mb.VERPR', 'LIKE', "%$search%");
                    })->orderBy($orderBy, $orderByDir)->paginate($perPage);
//                return new MarketingReportCollection($users);
            }
            else if($search !== '' && $request->filter === "CostInvoiceAmount"){
//                dd($search,'CostInvoiceAmount');
                $db = DB::connection('sqlsrv2')->getDatabaseName();
                $users = Invoice::join($db . '.dbo.MBEW as mb', function ($join) {
                    $join->on('mb.MATNR', '=', 'invoices.invoice_MATNR');
                    $join->on('mb.BWKEY', '=', 'invoices.invoice_plant');
                })
                    ->whereBetween('cost_invoice_amount', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                    ->where(function ($query) use ($search) {

                        $query->where('cost_invoice_amount', 'LIKE', "%$search%");
                    })->orderBy($orderBy, $orderByDir)->paginate($perPage);
//                return new MarketingReportCollection($users);
            }
            else{
//                dd($search);
                $db = DB::connection('sqlsrv2')->getDatabaseName();
                $users = Invoice::join($db . '.dbo.MBEW as mb', function ($join) {
                    $join->on('mb.MATNR', '=', 'invoices.invoice_MATNR');
                    $join->on('mb.BWKEY', '=', 'invoices.invoice_plant');
                })
                    ->whereBetween('invoices.created_at', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                    ->where(function ($query) use ($search) {

                        $query->where('invoice_plant', 'LIKE', "%$search%")
                            ->orWhere('invoice_date', 'LIKE', "%$search%")
                            ->orWhere('total_amount', 'LIKE', "%$search%")
                            ->orWhere('invoice_market_MAKTX', 'LIKE', "%$search%")
                            ->orWhere('mb.VERPR', 'LIKE', "%$search%")
                            ->orWhere('invoice_number', 'LIKE', "%$search%")
                            ->orWhere('cost_invoice_amount', 'LIKE', "%$search%");
                    })->orderBy($orderBy, $orderByDir)->paginate($perPage);
//                return new MarketingReportCollection($users);
            }

//        }
//        else {
//
//            $db = DB::connection('sqlsrv2')->getDatabaseName();
//            $orderdate = $request->data;
//            $newdate = new DateTime($orderdate);
//            $done = $newdate->format('d/m/Y');
//            $users = Invoice::select('invoices.invoice_plant', 'invoices.invoice_date',
//                    'invoices.total_amount', 'mb.VERPR',
//                    'invoices.invoice_market_MAKTX',
//                    'invoices.cost_invoice_amount',
//                    'invoices.invoice_number')
//                ->join($db . '.dbo.MBEW as mb', function ($join) {
//                    $join->on('mb.MATNR', '=', 'invoices.invoice_MATNR');
//                    $join->on('mb.BWKEY', '=', 'invoices.invoice_plant');
//                })
//                ->whereBetween('invoices.created_at', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
//                ->orderBy($orderBy, $orderByDir)->paginate($perPage);
//        }
        return new MarketingReportCollection($users);

    }

    public function InvoicePage(Request $request)
    {

        $db = DB::connection('sqlsrv2')->getDatabaseName();
        $date_from = $request->date_from;
        $newdate_from = new DateTime($date_from);
        $from = $newdate_from->format('Y/m/d');
        $date_to = $request->date_to;
        $newdate_to = new DateTime($date_to);
        $newdate = new DateTime($date_to);
        $to = $newdate_to->format('Y/m/d');

        $month = $newdate->format('m');
        $year = $newdate->format('Y');
        $day = $newdate->format('d');
//        dd($from, $to);
        if ($request->showAll) {
            $query = Invoice::orderBy('invoice_market_MAKTX', 'asc')->get();
            return new MarketingReportCollection($query);
        } else {

            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;
            if ($from !== $to && $searchValue === null) {
                $query = Invoice::join($db . '.dbo.MBEW as mb', function ($join) {
                    $join->on('mb.MATNR', '=', 'invoices.invoice_MATNR');
                    $join->on('mb.BWKEY', '=', 'invoices.invoice_plant');
                })
                    ->select('invoices.invoice_plant','invoices.invoice_date',
                        'invoices.total_amount','invoices.invoice_market_MAKTX',
                        'mb.VERPR','invoices.cost_invoice_amount','invoices.invoice_number')
                    ->whereBetween('invoices.created_at', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                return new MarketingReportCollection($query);
            }
            else if ($from === $to) {
//            dd('pasok');
//                if($request->filter === "PlantBranch"){
//                    dd('test');
//                }

                 $order = Invoice::join($db . '.dbo.MBEW as mb', function ($join) {
                    $join->on('mb.MATNR', '=', 'invoices.invoice_MATNR');
                    $join->on('mb.BWKEY', '=', 'invoices.invoice_plant');
                })
                    ->select('invoices.invoice_plant','invoices.invoice_date',
                        'invoices.total_amount','invoices.invoice_market_MAKTX',
                        'mb.VERPR','invoices.cost_invoice_amount','invoices.invoice_number')
                    ->where(DB::raw("MONTH(invoices.created_at)"), $month)
                    ->where(DB::raw("DAY(invoices.created_at)"), $day)
                    ->where(DB::raw("YEAR(invoices.created_at)"), $year)
                    ->orderBy($orderBy, $orderByDir)->paginate($perPage);

                return new MarketingReportCollection($order);
            }
            else {
                if ($searchValue) {
//                dd('test123');
                    $db = DB::connection('sqlsrv2')->getDatabaseName();
                    $users = Invoice::join($db . '.dbo.MBEW as mb', function ($join) {
                        $join->on('mb.MATNR', '=', 'invoices.invoice_MATNR');
                        $join->on('mb.BWKEY', '=', 'invoices.invoice_plant');
                    })
                        ->select('invoices.invoice_plant','invoices.invoice_date',
                            'invoices.total_amount','invoices.invoice_market_MAKTX',
                            'mb.VERPR','invoices.cost_invoice_amount','invoices.invoice_number')
                        ->whereBetween('invoices.created_at', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                        ->where(function ($query) use ($searchValue) {
                            $query->where('invoice_plant', 'LIKE', "%$searchValue%")
                                ->orWhere('invoice_date', 'LIKE', "%$searchValue%")
                                ->orWhere('total_amount', 'LIKE', "%$searchValue%")
                                ->orWhere('invoice_market_MAKTX', 'LIKE', "%$searchValue%")
                                ->orWhere('mb.VERPR', 'LIKE', "%$searchValue%")
                                ->orWhere('invoice_number', 'LIKE', "%$searchValue%")
                                ->orWhere('cost_invoice_amount', 'LIKE', "%$searchValue%");
                        })
                        ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                }
                else {
//                    dd('test');
                    $db = DB::connection('sqlsrv2')->getDatabaseName();
                    $orderdate = $request->data;
                    $newdate = new DateTime($orderdate);
                    $done = $newdate->format('d/m/Y');

                    $users = Invoice::select('invoices.invoice_plant','invoices.invoice_date',
                        'invoices.total_amount','invoices.invoice_market_MAKTX',
                        'mb.VERPR','invoices.cost_invoice_amount','invoices.invoice_number')
                    ->join($db . '.dbo.MBEW as mb', function ($join) {
                        $join->on('mb.MATNR', '=', 'invo.invoice_MATNR');
                        $join->on('mb.BWKEY', '=', 'invo.invoice_plant');
                    })

                        ->whereBetween('invoices.created_at', [Carbon::parse($from)->startOfDay(), Carbon::parse($to)->endOfDay()])
                        ->orderBy($orderBy, $orderByDir)->paginate($perPage);
                }
                return new MarketingReportCollection($users);
            }

        }

    }
}
