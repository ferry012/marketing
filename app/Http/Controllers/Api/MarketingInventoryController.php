<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\MarketingInventoryCollection;
use App\Plant;
use App\PlantUser;
use App\MaterialMaster;
use App\Model\Inventory;
use Illuminate\Http\Request;
use App\Model\MarketingInventory;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use DateTime;
use Carbon\Carbon;
use Spatie\Activitylog\Models\Activity;

class MarketingInventoryController extends Controller
{
    public function index()
    {

        $db = DB::connection('sqlsrv2')->getDatabaseName();

        $branch = DB::table('marketing_inventory')
            ->join($db . '.dbo.PLANT_BRANCHES as pb', 'marketing_inventory.marketing_plantbranch_WERKS', '=', 'pb.WERKS')
            ->join($db . '.dbo.MATERIAL_MASTER as mm', 'marketing_inventory.marketing_MATNR', '=', 'mm.MATNR')
            ->join($db . '.dbo.INVENTORY as inv', 'mm.MATNR', '=', 'inv.MATNR')
            ->join($db . '.dbo.MBEW as mb', 'inv.MATNR', '=', 'mb.MATNR')
            ->select('pb.NAME1', 'mm.MATNR', 'mm.MAKTX', 'mb.VERPR', 'inv.LABST', 'marketing_inventory.*')
            ->orderBy('marketing_inventory.id')
            ->get();
        return response()->json($branch);

    }


    public function store(Request $request)
    {


        $validator = $request->validate([

            'marketing_material' => ['required'],
            'marketing_description' => ['required'],
            'marketing_total_amount' => ['required'],
            'marketing_plant' => ['required'],

        ]);
        $plant = $request->all()['marketing_plant'];


        if (MarketingInventory::where('marketing_MATNR', '=', $request->marketing_material)
            ->where('marketing_plantbranch_WERKS', '=', $request->marketing_plant)->exists()) {

            return response()->json([

                'status' => 'warning',
                'message' => 'The Material is already exsist!'
            ]);

        } else {
            $host = gethostname();
            $name = auth()->user()->name;
            foreach ($plant as $p) {
                $query = MarketingInventory::updateOrCreate(['marketing_MATNR' => $request->marketing_material, 'marketing_plantbranch_WERKS' => $p],
                    ['marketing_MAKTX' => $request->marketing_description,
                        'marketing_total_amount' => $request->marketing_total_amount, 'market_date' => date('d/m/Y'), 'market_month' => date('F'),
                        'market_year' => date('Y'), 'valid_from' => date('d/m/Y', strtotime($request->valid_from)), 'valid_to' => date('d/m/Y', strtotime($request->valid_to))]);

                activity('Marketing - Create')
                    ->log('[WEB]' . ' ' . $name . ' ' . 'Has Created' . ' ' . $request->marketing_description . ' ' . 'Promo' . ' ' . 'To' . ' ' . $p
                        . ' ' . 'Using' . ' ' . $host . ' ' . 'Computer');
                $activity = Activity::all()->last();
            }

        }


    }


    public function show(Request $request, $id)
    {

        $intID = $id;
        $convertInt = (int)$intID;

        $marketing = DB::table('marketing_inventory')->where('id', $convertInt)->first();
        return response()->json($marketing);

    }

    public function update(Request $request, $id)
    {

        $data = array();
        $data['marketing_MATNR'] = $request->marketing_MATNR;
        $data['marketing_MAKTX'] = $request->marketing_MAKTX;
        $data['marketing_inv'] = $request->marketing_inv;
        $data['cost'] = $request->cost;
        $data['marketing_total_amount'] = $request->marketing_total_amount;
        $data['marketing_plantbranch_WERKS'] = $request->marketing_plantbranch_WERKS;
        MarketingInventory::where('id', $id)->update($data);


    }

    public function destroy($id)
    {
        $host = gethostname();
        $name = auth()->user()->name;
        $get = MarketingInventory::where('id', $id)->first();
        $MAKTX = $get->marketing_MAKTX;
        MarketingInventory::where('id', $id)->delete();
        activity('Marketing - Delete')
            ->log('[WEB]' . ' ' . $name . ' ' . 'Has Deleted a Material of' . ' ' . '[' . $MAKTX . ']' . ' ' . ' ' . 'Using' . ' ' . $host . ' ' . 'Computer');

    }

    public function updateMaterial(Request $request)
    {

        $host = gethostname();

        $validFromDate = $request->valid_from;
        $newdate = new DateTime($validFromDate);
        $validFromDateDone = $newdate->format('d/m/Y');

        $validToDate = $request->valid_to;
        $newdate = new DateTime($validToDate);
        $validToDateDone = $newdate->format('d/m/Y');
        $id = $request->id;
        $get = MarketingInventory::where('id', $id)->first();
        $MAKTX = $get->marketing_MAKTX;

        $data = array();
        $data['marketing_total_amount'] = $request->marketing_total_amount;
        $data['valid_from'] = $validFromDateDone;
        $data['valid_to'] = $validToDateDone;
        $total_amount = MarketingInventory::where('marketing_total_amount', '=', $request->marketing_total_amount)->exists();
        $from_logs = MarketingInventory::where('valid_from', '=', $validFromDateDone)->exists();
        $to_logs = MarketingInventory::where('valid_to', '=', $validToDateDone)->exists();
        //True if there is changes
        $name = auth()->user()->name;
        if (!$total_amount === true) {
            activity('Marketing - Update')
                ->log('[WEB]' . ' ' . $name . ' ' . 'Has Updated' . ' ' . 'The Material Total Target of' . ' ' . '[' . $MAKTX . ']' . ' '
                    . 'To' . ' ' . $request->marketing_total_amount . ' ' . 'Using' . ' ' . $host . ' ' . 'Computer');
        } else if (!$from_logs === true) {
            activity('Marketing - Update')
                ->log('[WEB]' . ' ' . $name . ' ' . 'Has Updated The' . ' ' . 'Promo Starts of' . ' ' . '[' . $MAKTX . ']' . ' ' . 'From' . ' ' . $validFromDateDone
                    . ' ' . 'Using' . ' ' . $host . ' ' . 'Computer');
        } else if (!$to_logs === true) {
            activity('Marketing - Update')
                ->log('[WEB]' . ' ' . $name . ' ' . 'Has Updated The' . ' ' . 'Promo Ends of' . ' ' . '[' . $MAKTX . ']' . ' ' . 'To' . ' ' . $validToDateDone
                    . ' ' . 'Using' . ' ' . $host . ' ' . 'Computer');
        }

        $query = MarketingInventory::where('id', $id)->update($data);
        $activity = Activity::all()->last();
        return $query;

    }

    public function masterTotal(Request $request)
    {

        if ((float)$request->total_amount == 0) {

            $user = auth()->user();

            $plant = PlantUser::where('users_id', '=', $user->id)->first();

            $marketing = MarketingInventory::select('marketing_inventory.*')
                ->where('marketing_plantbranch_WERKS', '=', $plant->WERKS)
                ->where('marketing_total_amount', '<=', (float)$request->total_amount)
                ->get();
        } else {
            $db = DB::connection('sqlsrv2')->getDatabaseName();
            $user = auth()->user();
            $plant = PlantUser::where('users_id', '=', $user->id)->first();
            $todayDate = date('d/m/Y');
            $date = Carbon::now()->format('d/m/Y');
            $marketing = DB::table('marketing_inventory as a')
                ->select('inv.LABST', 'a.marketing_total_amount', 'a.marketing_plantbranch_WERKS', 'a.valid_to','a.valid_from', 'a.marketing_MAKTX', 'a.marketing_MATNR')
                ->join($db . '.dbo.INVENTORY as inv', function ($join) {
                    $join->on('inv.MATNR', '=', 'a.marketing_MATNR');
                    $join->on('inv.WERKS', '=', 'a.marketing_plantbranch_WERKS')
                        ->where('inv.LABST', '>', 0)
                        ->where('inv.LABST', '!=', 0);
                })
                ->where(function ($q) use ($request) {
                    $q->where('a.marketing_total_amount', '<=', (int)$request->total_amount);
                })
                ->where(function ($q) use ($plant) {
                    $q->where('a.marketing_plantbranch_WERKS', '=', $plant->WERKS);
                })
//                ->where(function ($q) use ($date) {
//                    $q->whereDate('a.valid_to', '>=', $date)
//                        ->orwhereDate('a.valid_from','<=',$date);
//                })

//                ->where(function ($q) use ($date) {
//                    $q->where(
//                        DB::raw("convert(varchar, a.valid_to, 103)")
//                        , '>=', $date)
//                        ->where(
//                            DB::raw("convert(varchar, a.valid_from, 103)")
//                    , '<=', $date);
//                })
                ->where(function ($q) use ($date) {
                    $q->where(
                        DB::raw("convert(varchar, a.valid_to, 103)")
                        , '>=', $date)
                        ->orwhere(
                            DB::raw("convert(varchar, a.valid_from, 103)")
                    , '<=', $date);
                })
                ->groupBy('inv.LABST', 'a.marketing_total_amount', 'a.marketing_plantbranch_WERKS', 'a.valid_to','a.valid_from','a.marketing_MAKTX', 'a.marketing_MATNR')
                ->get();
        }

        return response()->json($marketing);

    }

    public function getPlant(Request $request)
    {


        return $request->plantbranch_id;


    }

    public function master(Request $request)
    {

        $showAll = $request->get('showAll');
        $searchCode = $request->get('code');
        $plant = $request->all()['plantbranch_id'];
        $clone = explode(',', $plant);

        $product = Inventory::join('MATERIAL_MASTER as mm', 'INVENTORY.MATNR', '=', 'mm.MATNR')
            ->join('MBEW as mb', 'INVENTORY.MATNR', '=', 'mb.MATNR')
            ->where('INVENTORY.MATNR', 'LIKE', "%$searchCode%")
            ->whereIn('INVENTORY.WERKS', $clone)
            ->distinct('INVENTORY.MATNR')
            ->where('mm.MTART', 'LIKE', '%ZPRO%')
            ->select('INVENTORY.MATNR', 'mm.MAKTX')
            ->groupBy('INVENTORY.MATNR', 'mm.MAKTX')
            ->distinct('inv.MATNR')
            ->get();
        return $product;
//        $cloneSearch = explode(',',$searchCode);
//        dd($cloneSearch);
//        dd($searchCode);

//        $material = $request->all()['material_matnr'];
//        $clone = explode(',', $material);

//         dd($clone);
        // $search = $request->get(['code']);

//orginial
//        $product = MaterialMaster::select('inv.LABST', 'inv.MATNR', 'mb.VERPR', 'MATERIAL_MASTER.MAKTX')
//            ->join('INVENTORY as inv', 'MATERIAL_MASTER.MATNR', '=', 'inv.MATNR')
//            ->join('MBEW as mb', 'inv.MATNR', '=', 'mb.MATNR')
//            ->where('inv.MATNR', 'LIKE', "%$searchCode%")
//            ->whereIn('inv.WERKS', $clone)
//            ->distinct('inv.MATNR')
//            ->where('MATERIAL_MASTER.MTART', 'LIKE', '%ZPRO%')
//            ->groupBy('inv.LABST', 'inv.MATNR', 'mb.VERPR', 'MATERIAL_MASTER.MAKTX')
//            ->get();


//        $product = MaterialMaster::select('inv.LABST', 'MATERIAL_MASTER.MATNR', 'mb.VERPR', 'MATERIAL_MASTER.MAKTX')
//            ->join('INVENTORY as inv', 'MATERIAL_MASTER.MATNR', '=', 'inv.MATNR')
//            ->join('MBEW as mb', 'inv.MATNR', '=', 'mb.MATNR')
//            ->where('MATERIAL_MASTER.MATNR', 'LIKE', "%$searchCode%")
//            ->whereIn('inv.WERKS', $clone)
//            ->distinct('inv.WERKS')
//            ->where('MATERIAL_MASTER.MTART', 'LIKE', '%ZPRO%')
//            ->groupBy('inv.LABST', 'MATERIAL_MASTER.MATNR', 'mb.VERPR', 'MATERIAL_MASTER.MAKTX')
//            ->get();

//        $product = MaterialMaster::select('inv.LABST', 'MATERIAL_MASTER.MATNR', 'mb.VERPR', 'MATERIAL_MASTER.MAKTX','pb.NAME1','pb.WERKS')
//            ->join('INVENTORY as inv', 'MATERIAL_MASTER.MATNR', '=', 'inv.MATNR')
//            ->join('MBEW as mb', 'inv.MATNR', '=', 'mb.MATNR')
//            ->join('PLANT_BRANCHES as pb', 'inv.WERKS','=','pb.WERKS')
//            ->where('pb.WERKS', 'LIKE', "%$searchCode%")
//            ->whereIn('inv.MATNR', $clone)
//            ->distinct('inv.WERKS')
//            ->where('MATERIAL_MASTER.MTART', 'LIKE', '%ZPRO%')
//            ->groupBy('inv.LABST', 'MATERIAL_MASTER.MATNR', 'mb.VERPR', 'MATERIAL_MASTER.MAKTX','pb.NAME1','pb.WERKS')
//            ->get();
//dd($product);
        // $product = MaterialMaster::select('inv.LABST','MATERIAL_MASTER.MATNR','mb.VERPR','MATERIAL_MASTER.MAKTX')
        //       ->join('INVENTORY as inv','MATERIAL_MASTER.MATNR','=','inv.MATNR')
        //       ->join('MBEW as mb','inv.MATNR','=','mb.MATNR')
        //       ->where('MATERIAL_MASTER.MATNR','LIKE',"%{ $search }%")
        //       ->where('inv.WERKS','=',$request->plantbranch_id)
        //       ->where('MATERIAL_MASTER.MTART','LIKE','%ZPRO%')
        //       ->groupBy('inv.LABST','MATERIAL_MASTER.MATNR','mb.VERPR','MATERIAL_MASTER.MAKTX')
        //       ->get();

// dd($product);

//        return response()->json($product);
        // dd($searchCode);
//        if ($showAll == '1') {

        // $product = MaterialMaster::select('inv.LABST','MATERIAL_MASTER.MATNR','mb.VERPR')
        // ->join('INVENTORY as inv','MATERIAL_MASTER.MATNR','=','inv.MATNR')
        // ->join('MBEW as mb','inv.MATNR','=','mb.MATNR')
        // ->where('MATERIAL_MASTER.MATNR','LIKE',"%$searchCode%")
        // ->where('inv.WERKS','=',$request->plantbranch_id)
        // ->where('MATERIAL_MASTER.MTART','LIKE','%ZPRO%')
        // // ->value('MATERIAL_MASTER.MTART','==','ZPRO')
        // ->groupBy('inv.LABST','MATERIAL_MASTER.MATNR','mb.VERPR')
        // // ->orderBy('MATNR_ID','asc')
        // ->get();
        // $db = DB::connection('sqlsrv2')->getDatabaseName();

        //  $product = Inventory::select('INVENTORY.LABST','mm.MATNR','mb.VERPR','mm.*')
        //           ->join('MATERIAL_MASTER as mm','INVENTORY.MATNR','=','mm.MATNR')
        //           ->join('MBEW as mb','mm.MATNR','=','mb.MATNR')
        //           ->where('MATNR','LIKE',"%$searchCode%")
        //           ->orderBy('MATNR_ID','asc')->get();

        //  $bawal = [2312, 2322, 2332, 2342 ,2352 ,9012 ,2362];
        // $check = Plantbranches::select('PLANT_BRANCHES.WERKS','PLANT_BRANCHES.NAME1','in.LABST')
        // ->join('INVENTORY as in', 'PLANT_BRANCHES.WERKS','=','in.WERKS')
        // ->where('in.LGORT','!=',$bawal)
        // ->where('PLANT_BRANCHES.WERKS','LIKE',"%$searchCode%")
        // -get();


        // $product = MaterialMaster::select('inv.LABST','MATERIAL_MASTER.MATNR','mb.VERPR','MATERIAL_MASTER.*')
        // ->join('INVENTORY as inv','MATERIAL_MASTER.MATNR','=','inv.MATNR')
        // ->join('MBEW as mb','inv.MATNR','=','mb.MATNR')
        // ->where('MATERIAL_MASTER.MATNR','LIKE',"%$searchCode%")
        // ->where('MATERIAL_MASTER.MTART','LIKE','ZPRO')
        // // ->value('MATERIAL_MASTER.MTART','==','ZPRO')

        // ->orderBy('MATNR_ID','asc')
        // ->get();
        // ->where('MATERIAL_MASTER.MTART','==','ZPRO','&&','MATERIAL_MASTER.MATNR','LIKE',"%$searchCode%")
        // ->orderBy('MATNR_ID','asc')


        // return response()->json($product);
//  $product = MaterialMaster::where('MATNR','LIKE',"%$searchCode%")
//               ->join($db.'.dbo.INVENTORY as in','MATERIAL_MASTER.MATNR','=','in.MATNR')
//               ->join($db.'.dbo.MBEW as mb','in.MATNR','=','mb.MATNR')
//               ->select('MATERIAL_MASTER.MATNR','MATERIAL_MASTER.MAKTX','mb.VERPR','in.LABST')
//               ->orderBy('MATNR_ID','asc')->get();

        //  $product = MaterialMaster::where('MATNR','LIKE',"%$searchCode%")
        //              ->select('MATERIAL_MASTER.*')
        //              ->orderBy('MATNR_ID','asc')->get();

//        } else {
        // $product = MaterialMaster::where('MATNR','LIKE',"%$searchCode%")
        //    ->select('MATERIAL_MASTER.*')
        //    ->orderBy('MATNR_ID','asc')->get();


        // $product = Product::join('A801 as a','a.MATNR','MATERIAL_MASTER.MATNR')
        //     ->join('KONP as k','k.KNUMH','a.KNUMH')
        //     ->where('MAKTX',"$searchCode")
        //     ->orwhere('EAN11',"$searchCode")
        //     ->select('k.KBETR','MATERIAL_MASTER.*')
        //     ->orderBy('MAKTX','asc')->get();
//        }

        //  return $product;
        //return response()->json($product);
        // dd($product);

    }
}
