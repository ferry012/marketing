<?php

namespace App\Http\Controllers\Api;

use App\PlantUser;
use App\Model\Invoice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class InvoiceControllerPage extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $plant = PlantUser::where('users_id', '=', $user->id)->first();
        $today = Carbon::today();
        $todayFormat = $today->format('Y/m/d');
//        dd($todayFormat);
        return Invoice::where('invoice_plant', '=', $plant->WERKS)
            ->whereDate('created_at',$todayFormat)
            ->latest()
            ->paginate(5);

        // return DB::table('invoices')->paginate(5);
    }

    public function InvoiceSearch(Request $request) //search
    {

        if ($search = $request->search) {
            $user = auth()->user();
            $plant = PlantUser::where('users_id', '=', $user->id)->first();

            $users = Invoice::where('invoice_plant','=',$plant->WERKS)->where(function ($query) use ($search) {
//                $user = auth()->user();
//                $plant = PlantUser::where('users_id', '=', $user->id)->first();
//                dd($plant->WERKS);
//                $query->where('invoice_plant', '=', $plant->WERKS)
                   $query ->orWhere('invoice_number', 'LIKE', "%$search%")
                    ->orWhere('total_amount', 'LIKE', "%$search%")
                    ->orWhere('invoice_market_MAKTX', 'LIKE', "%$search%")
                    ->orWhere('invoice_MATNR', 'LIKE', "%$search%")
                    ->orWhere('qty', 'LIKE', "%$search%");

            })->paginate(100);
        } else {
            $user = auth()->user();
            $plant = PlantUser::where('users_id', '=', $user->id)->first();
            $users = Invoice::where('invoice_plant', '=', $plant->WERKS)
                ->latest()
                ->paginate(5);
        }
        return response()->json($users);

        //    if ($search = $request->search){

        //        $users = DB::table('invoices')->where(function($query) use ($search){

        //            $query->where('invoice_number','LIKE',"%$search%")
        //            ->orWhere('total_amount','LIKE',"%$search%")
        //            ->orWhere('invoice_market_MAKTX','LIKE',"%$search%")
        //            ->orWhere('invoice_market_inventory','LIKE',"%$search%")
        //            ->orWhere('qty','LIKE',"%$search%");

        //        })->paginate(30);
        //    }
        //    else{
        //     $users = DB::table('invoices')->paginate(5);
        //    }
        //    return response()->json($users);


    }
}
