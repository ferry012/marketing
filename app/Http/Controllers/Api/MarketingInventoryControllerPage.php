<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\MarketingInventoryCollection;
use App\Model\Plantbranches;
use App\PlantUser;
use Illuminate\Http\Request;
use App\Model\MarketingInventory;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MarketingInventoryControllerPage extends Controller
{
    public function index(Request $request)
    {

        $db = DB::connection('sqlsrv2')->getDatabaseName();
        if($request->showAll) {
            $query = MarketingInventory::orderBy('marketing_MATNR','asc')->get();
        }else {
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;

            if($searchValue !== '' && $request->filter === "MaterialNumber"){
                $query = MarketingInventory::select('inv.LABST', 'mb.VERPR', 'marketing_inventory.*')
                    ->join($db . '.dbo.INVENTORY as inv', function ($join) {
                        $join->on('inv.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                        $join->on('inv.WERKS', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                    })
                    ->join($db . '.dbo.MBEW as mb', function ($join) {
                        $join->on('mb.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                        $join->on('mb.BWKEY', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                    })
                    ->where('marketing_MATNR', 'LIKE', "%$searchValue%")
                    ->paginate($perPage);
            }
           else if($searchValue !== '' && $request->filter === "MaterialDescription"){
               $query = MarketingInventory::select('inv.LABST', 'mb.VERPR', 'marketing_inventory.*')
                   ->join($db . '.dbo.INVENTORY as inv', function ($join) {
                       $join->on('inv.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                       $join->on('inv.WERKS', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                   })
                   ->join($db . '.dbo.MBEW as mb', function ($join) {
                       $join->on('mb.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                       $join->on('mb.BWKEY', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                   })
                   ->where('marketing_MAKTX', 'LIKE', "%$searchValue%")
                   ->paginate($perPage);
            }
           else if($searchValue !== '' && $request->filter === "MaterialInventory"){
               $query = MarketingInventory::select('inv.LABST', 'mb.VERPR', 'marketing_inventory.*')
                   ->join($db . '.dbo.INVENTORY as inv', function ($join) {
                       $join->on('inv.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                       $join->on('inv.WERKS', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                   })
                   ->join($db . '.dbo.MBEW as mb', function ($join) {
                       $join->on('mb.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                       $join->on('mb.BWKEY', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                   })
                   ->where('inv.LABST', 'LIKE', "%$searchValue%")
                   ->paginate($perPage);
            }
           else if($searchValue !== '' && $request->filter === "Costing"){
               $query = MarketingInventory::select('inv.LABST', 'mb.VERPR', 'marketing_inventory.*')
                   ->join($db . '.dbo.INVENTORY as inv', function ($join) {
                       $join->on('inv.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                       $join->on('inv.WERKS', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                   })
                   ->join($db . '.dbo.MBEW as mb', function ($join) {
                       $join->on('mb.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                       $join->on('mb.BWKEY', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                   })
                   ->where('mb.VERPR', 'LIKE', "%$searchValue%")
                   ->paginate($perPage);
            }
           else if($searchValue !== '' && $request->filter === "PromoStarts"){
               $query = MarketingInventory::select('inv.LABST', 'mb.VERPR', 'marketing_inventory.*')
                   ->join($db . '.dbo.INVENTORY as inv', function ($join) {
                       $join->on('inv.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                       $join->on('inv.WERKS', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                   })
                   ->join($db . '.dbo.MBEW as mb', function ($join) {
                       $join->on('mb.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                       $join->on('mb.BWKEY', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                   })
                   ->where('valid_from', 'LIKE', "%$searchValue%")
                   ->paginate($perPage);
            }
           else if($searchValue !== '' && $request->filter === "PromoEnds"){
               $query = MarketingInventory::select('inv.LABST', 'mb.VERPR', 'marketing_inventory.*')
                   ->join($db . '.dbo.INVENTORY as inv', function ($join) {
                       $join->on('inv.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                       $join->on('inv.WERKS', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                   })
                   ->join($db . '.dbo.MBEW as mb', function ($join) {
                       $join->on('mb.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                       $join->on('mb.BWKEY', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                   })
                   ->where('valid_to', 'LIKE', "%$searchValue%")
                   ->paginate($perPage);
            }
           else if($searchValue !== '' && $request->filter === "TotalTarget"){
               $query = MarketingInventory::select('inv.LABST', 'mb.VERPR', 'marketing_inventory.*')
                   ->join($db . '.dbo.INVENTORY as inv', function ($join) {
                       $join->on('inv.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                       $join->on('inv.WERKS', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                   })
                   ->join($db . '.dbo.MBEW as mb', function ($join) {
                       $join->on('mb.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                       $join->on('mb.BWKEY', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                   })
                   ->where('marketing_total_amount', 'LIKE', "%$searchValue%")
                   ->paginate($perPage);
            }
           else if($searchValue !== '' && $request->filter === "PlantBranch"){
               $query = MarketingInventory::select('inv.LABST', 'mb.VERPR', 'marketing_inventory.*')
                   ->join($db . '.dbo.INVENTORY as inv', function ($join) {
                       $join->on('inv.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                       $join->on('inv.WERKS', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                   })
                   ->join($db . '.dbo.MBEW as mb', function ($join) {
                       $join->on('mb.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                       $join->on('mb.BWKEY', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                   })
                   ->where('marketing_total_amount', 'LIKE', "%$searchValue%")
                   ->paginate($perPage);
            }
           else{
               $query = MarketingInventory::select('inv.LABST', 'mb.VERPR', 'marketing_inventory.*')
                   ->join($db . '.dbo.INVENTORY as inv', function ($join) {
                       $join->on('inv.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                       $join->on('inv.WERKS', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                   })
                   ->join($db . '.dbo.MBEW as mb', function ($join) {
                       $join->on('mb.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                       $join->on('mb.BWKEY', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                   })
                   ->where('marketing_MATNR', 'LIKE', "%$searchValue%")
                   ->orWhere('marketing_MAKTX', 'LIKE', "%$searchValue%")
                   ->orWhere('marketing_plantbranch_WERKS', 'LIKE', "%$searchValue%")
                   ->orWhere('marketing_total_amount', 'LIKE', "%$searchValue%")
                   ->orWhere('valid_from', 'LIKE', "%$searchValue%")
                   ->orWhere('valid_to', 'LIKE', "%$searchValue%")
                   ->orWhere('inv.LABST', 'LIKE', "%$searchValue%")
                   ->orWhere('mb.VERPR', 'LIKE', "%$searchValue%")
                   ->orderBy($orderBy, $orderByDir)
                   ->paginate($perPage);
           }

//            return $query;

        }

        return new MarketingInventoryCollection($query);
//        return response()->json($query);
    }

    public function searchMarketing(Request $request) //search
    {

        if ($search = $request->search) {
            $db = DB::connection('sqlsrv2')->getDatabaseName();
            $users = MarketingInventory::join($db . '.dbo.INVENTORY as inv', function ($join) {
                $join->on('inv.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                $join->on('inv.WERKS', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
            })
//            ->join('VCYSYS820.dbo.MBEW as mb','a.marketing_MATNR','=','mb.MATNR')
                ->join($db . '.dbo.MBEW as mb', function ($join) {
                    $join->on('mb.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                    $join->on('mb.BWKEY', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
                })
                ->where(function ($query) use ($search) {

                    $query->where('marketing_MATNR', 'LIKE', "%$search%")
                        ->orWhere('marketing_MAKTX', 'LIKE', "%$search%")
                        ->orWhere('marketing_plantbranch_WERKS', 'LIKE', "%$search%")
                        ->orWhere('marketing_total_amount', 'LIKE', "%$search%")
                        ->orWhere('valid_from', 'LIKE', "%$search%")
                        ->orWhere('valid_to', 'LIKE', "%$search%")
                        ->orWhere('inv.LABST', 'LIKE', "%$search%")
                        ->orWhere('mb.VERPR', 'LIKE', "%$search%");
                })->paginate(100);
        } else {
//            $users = MarketingInventory::paginate(5);
            $db = DB::connection('sqlsrv2')->getDatabaseName();
            $users = DB::table('marketing_inventory as a')
                ->select('inv.LABST', 'a.marketing_MATNR', 'a.marketing_plantbranch_WERKS', 'a.marketing_MAKTX', 'mb.VERPR', 'a.marketing_total_amount', 'a.*')
                ->join($db . '.dbo.INVENTORY as inv', function ($join) {
                    $join->on('inv.MATNR', '=', 'a.marketing_MATNR');
                    $join->on('inv.WERKS', '=', 'a.marketing_plantbranch_WERKS');
                })
//            ->join('VCYSYS820.dbo.MBEW as mb','a.marketing_MATNR','=','mb.MATNR')
                ->join($db . '.dbo.MBEW as mb', function ($join) {
                    $join->on('mb.MATNR', '=', 'a.marketing_MATNR');
                    $join->on('mb.BWKEY', '=', 'a.marketing_plantbranch_WERKS');
                })
//            ->where('inv.MATNR','=','marketing_inventory.marketing_MATNR')
//            ->where('inv.WERKS','=','marketing_inventory.marketing_plantbranch_WERKS')
//                ->orderBy('a.marketing_MATNR', 'asc')
                ->latest()
                ->paginate(5);
        }
        return response()->json($users);


    }

    public function searchMarketingInv(Request $request)
    {
        if ($search = $request->search) {
            $db = DB::connection('sqlsrv2')->getDatabaseName();
            $user = auth()->user();
            $plant = PlantUser::where('users_id', '=', $user->id)->first();

            $users = MarketingInventory::join($db . '.dbo.INVENTORY as inv', function ($join) {
                $join->on('inv.MATNR', '=', 'marketing_inventory.marketing_MATNR');
                $join->on('inv.WERKS', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
            })
//            ->join('VCYSYS820.dbo.MBEW as mb','a.marketing_MATNR','=','mb.MATNR')

                ->where('marketing_inventory.marketing_plantbranch_WERKS', '=', $plant->WERKS)
                ->where(function ($query) use ($search) {

                    $query->where('marketing_MATNR', 'LIKE', "%$search%")
                        ->orWhere('marketing_MAKTX', 'LIKE', "%$search%")
                        ->orWhere('marketing_total_amount', 'LIKE', "%$search%")
                        ->orWhere('valid_to', 'LIKE', "%$search%")
                        ->orWhere('inv.LABST', 'LIKE', "%$search%");
                })->paginate(5);
        } else {
            $user = auth()->user();
            $plant = PlantUser::where('users_id', '=', $user->id)->first();
            $db = DB::connection('sqlsrv2')->getDatabaseName();
            $users = DB::table('marketing_inventory as a')
                ->select('inv.LABST', 'a.marketing_MATNR', 'a.marketing_MAKTX', 'a.marketing_total_amount', 'a.valid_to')
                ->join($db . '.dbo.INVENTORY as inv', function ($join) {
                    $join->on('inv.MATNR', '=', 'a.marketing_MATNR');
                    $join->on('inv.WERKS', '=', 'a.marketing_plantbranch_WERKS');
                })
                ->where('a.marketing_plantbranch_WERKS', '=', $plant->WERKS)
//            ->join('VCYSYS820.dbo.MBEW as mb','a.marketing_MATNR','=','mb.MATNR')

//            ->where('inv.MATNR','=','marketing_inventory.marketing_MATNR')
//            ->where('inv.WERKS','=','marketing_inventory.marketing_plantbranch_WERKS')
//                ->orderBy('a.marketing_MATNR', 'asc')
                ->latest()
                ->paginate(5);
        }
        return response()->json($users);

    }

    public function getInvoice(Request $request)
    {
        $db = DB::connection('sqlsrv2')->getDatabaseName();
        $user = auth()->user();
        $plant = PlantUser::where('users_id', '=', $user->id)->first();
        return MarketingInventory::join($db . '.dbo.INVENTORY as inv', function ($join) {
            $join->on('inv.MATNR', '=', 'marketing_inventory.marketing_MATNR');
            $join->on('inv.WERKS', '=', 'marketing_inventory.marketing_plantbranch_WERKS');
        })
            ->where('marketing_inventory.marketing_plantbranch_WERKS', '=', $plant->WERKS)
            ->paginate(5);
    }
}
