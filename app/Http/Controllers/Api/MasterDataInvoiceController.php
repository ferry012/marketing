<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Invoice;
use App\PlantUser;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MasterDataInvoiceController extends Controller
{
    public function getInvoice(){

        return Invoice::latest()
            ->paginate(10);
    }
    public function getSearchInvoice(Request $request) //search
    {
        if ($search = $request->search){
            $users = Invoice::where(function($query) use ($search){
                $query->where('invoice_plant','LIKE',"%$search%")
                    ->orWhere('invoice_number','LIKE',"%$search%")
                    ->orWhere('total_amount','LIKE',"%$search%")
                    ->orWhere('invoice_MATNR','LIKE',"%$search%")
                    ->orWhere('invoice_market_MAKTX','LIKE',"%$search%")
                    ->orWhere('qty','LIKE',"%$search%")
                    ->orWhere('cashier_name','LIKE',"%$search%");
            })->paginate(100);
        }
        else{
            $users = Invoice::latest()
                ->paginate(10);

        }
        return response()->json($users);




    }
}
