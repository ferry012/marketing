<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\check_excel;
use Illuminate\Http\Request;

class CheckExcelController extends Controller
{
    public function index(Request $request)
    {
        return check_excel::paginate(5);
    }
}
