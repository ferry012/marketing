<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use charlieuki\ReceiptPrinter\ReceiptPrinter as ReceiptPrinter;

class PrintPosController extends Controller
{
    
   public function printPos(Request $request){
         // Set params
         $vat = DB::table('vat')->get();
         $allOrders = DB::table('orders')->get();
         $allOrdersDetails = DB::table('orders')->get();
        //  $mid = '123123456';
         $store_name = 'TRIUMPH HOME DEPOT';
         $operated_by = 'VCY SALES CORPORATION';
         $store_address = 'Araneta St., Brgy.Sum-ag, Bacolod City';
         $vatreg_tin = '005-430-314-014';
        //  $store_phone = '1234567890';
        //  $store_email = 'vcy@email.com';
        //  $store_website = 'yourmart.com';
        //  $tax_percentage = 10;
        //  $transaction_id = 'TX123ABC456';
        $terminal = '19-VCYSUMDPOS02';
        $sN = '5asd65216asda';
        $min = '6546265746312637';
        $permit = 'FP022020-077-0250640-00014';

     // Set items     

    //  $data = array();
    //  $data['customer_id'] = $request->customer_id;
    //  $data['qty'] = $request->qty1;
    //  $data['sub_total'] = $request->subtotal;
    //  $data['vat'] = $request->vat;
    //  $data['total'] = $request->total;
    //  $data['pay'] = $request->pay;
    //  $data['payby'] = $request->payby;
    //  $data['discount'] = $request->discTotal;
    //  $data['order_date'] = date('d/m/Y');
    //  $data['order_month'] = date('F');
    //  $data['order_year'] = date('Y');

    //  $contents = DB::table('pos')->get();
   $items = [
        [
            'customer_id' => $request->customer_id,
            'qty' => $request->qty1,
            'sub_total' => $request->subtotal, 
            'vat' => $request->vat, 
            'total' => $request->total, 
            'pay' => $request->pay,
            'payby' => $request->payby,
            'discount' => $request->discTotal,    
        ],

    ];

    // Init printer
    $printer = new ReceiptPrinter;
    $printer->init(
        config('receiptprinter.connector_type'),
        config('receiptprinter.connector_descriptor')
    );

    // Set store info
    $printer->setStore($store_name, $operated_by, $store_address, $vatreg_tin,$sN,$min,$permit);

    // Add items
    foreach ($items as $item) {
        $printer->addItem(
            $item['customer_id'],
            $item['qty'],
            $item['sub_total'],  
            $item['vat'],
            $item['total'],
            $item['pay'],
            $item['payby'],
            $item['discount']
        );
    }
    // Set tax
    $printer->setTax($vat->vat);

    // Calculate total
    $printer->calculateSubTotal();
    $printer->calculateGrandTotal();

    // Set transaction ID
    $printer->setTransactionID($terminal);

    // Set qr code
    $printer->setQRcode([
        'tid' => $terminal,
    ]);

    // Print receipt
    $printer->printReceipt();
}
   
}

