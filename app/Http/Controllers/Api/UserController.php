<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\PlantUser;
use App\Model\Plantbranches;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Gate;
use App\Http\Resources\UserCollection;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return UserCollection
     */
    public function index(Request $request)
    {

        if (! Gate::allows('user_view')) {
            return abort(403);
        }

        if($request->showAll) {
            $query = User::with('roles')->orderBy('name','asc')->get();
        }else{

            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;
//            $db = DB::connection('sqlsrv2')->getDatabaseName();

            // $query = User::with('roles')->where('name', 'LIKE', "%$searchValue%")
            //     ->orwhere('email', 'LIKE', "%$searchValue%")
            //     ->orwhereDate('created_at', 'LIKE', "%$searchValue%")
            //     ->select('pb.NAME1','users.*')
            //     ->join($db.'.dbo.PLANT_BRANCHES as pb','users.plantbranch_id','=','pb.WERKS')
            //     ->orderBy($orderBy, $orderByDir)->paginate($perPage);
            $db = DB::connection('sqlsrv2')->getDatabaseName();
            $query = User::with('roles')
                ->select('pb.NAME1','pb.WERKS','users.*')
                ->join('plantbranch','users.id','=','plantbranch.users_id')
                ->join($db.'.dbo.PLANT_BRANCHES as pb','plantbranch.WERKS','=','pb.WERKS')
                ->where('users.name', 'LIKE', "%$searchValue%")
                ->orwhere('users.email', 'LIKE', "%$searchValue%")
                ->orwhereDate('users.created_at', 'LIKE', "%$searchValue%")
                ->orderBy($orderBy, $orderByDir)->paginate($perPage);
        }

        return new UserCollection($query);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if (! Gate::allows('user_create')) {
            return abort(403);
        }

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255','unique:users','regex:/(^[A-Za-z0-9-_]+$)+/'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'roles' => ['required'],
            // 'plants' => ['required'],/
            'WERKS' => ['required'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $host = gethostname();
        $nameUser = auth()->user()->name;

       $user = User::create($request->all());
       $plantuser = PlantUser::create([
           'WERKS'=>$request->WERKS,
        //    'WERKS'=>$request->plants,
           'users_id'=>$user->id,
       ]);
       $roles = $request->input('roles') ? $request->input('roles') : [];
       $user->assignRole($roles);

       if ($user) {
           activity('User - Create')
               ->log('[WEB]'.' '.$nameUser.' '.'Has Created a User'.' '.$user->name.' '
                   .'Using'.' '.$host.' '.'Computer');
           return response()->json([
               'status' => 'success',
               'message' => 'User successfully created'
           ]);

       }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return UserResource
     */
    public function show($id)
    {
        if (! Gate::allows('user_view')) {
            return abort(403);
        }
        // return new UserResource(User::with('roles')->find($id));
        // $query = User::with('roles')
        // ->select('pb.NAME1','pb.WERKS','users.*')
        // ->join('plantbranch','users.id','=','plantbranch.users_id')
        // ->join('VCYSYS820.dbo.PLANT_BRANCHES as pb','plantbranch.WERKS','=','pb.WERKS')
        // ->where('users.name', 'LIKE', "%$searchValue%")
        // ->orwhere('users.email', 'LIKE', "%$searchValue%")
        // ->orwhereDate('users.created_at', 'LIKE', "%$searchValue%")
        // ->orderBy($orderBy, $orderByDir)->paginate($perPage);
        $query = User::with('roles')
            ->select('plantbranch.WERKS','users.*')
            ->join('plantbranch','users.id','=','plantbranch.users_id')
            ->find($id);
        return new UserResource($query);
//        return new UserResource(User::with('roles')->find($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return UserResource
     */
    public function edit($id)
    {
        if (! Gate::allows('user_edit')) {
            return abort(403);
        }
        $db = DB::connection('sqlsrv2')->getDatabaseName();
        // $user = User::with('roles')->find($id);
        // $plant = PlantUser::select('pbar.WERKS','pbar.NAME1','plantbranch.*')
        // ->join('VCYSYS820.dbo.PLANT_BRANCHES as pbar','plantbranch.WERKS','=','pbar.WERKS')
        // ->get();
        $db = DB::connection('sqlsrv2')->getDatabaseName();
        $query = User::with('roles')
                ->select('pb.NAME1','plantbranch.WERKS','plantbranch.WERKS','users.*')
                ->join('plantbranch','users.id','=','plantbranch.users_id')
                ->join($db.'.dbo.PLANT_BRANCHES as pb','plantbranch.WERKS','=','pb.WERKS')
                ->find($id);

        // $query = User::with('roles')
        //         ->select('pb.NAME1','pb.WERKS','plantbranch.WERKS','users.*')
        //         ->join('plantbranch','users.id','=','plantbranch.users_id')
        //         ->join('VCYSYS820.dbo.PLANT_BRANCHES as pb','plantbranch.WERKS','=','pb.WERKS')
        //         ->find($id);


         // ->join('VCYSYS820.dbo.PLANT_BRANCHES as pbar','plantbranch.WERKS','=','pbar.WERKS')
        $get =  User::with('roles')
            ->where('users.id','=',$id)
            ->first();
        $getName =  $get->name;
        $host = gethostname();
        $nameUser = auth()->user()->name;
        activity('User - Update')
            ->log('[WEB]'.' '.$nameUser.' '.'Has Updated a User'.' '.$getName.' '
                .'Using'.' '.$host.' '.'Computer');
        return new UserResource($query);
        // return new UserResource(User::with('roles')->find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {

        if (! Gate::allows('user_edit')) {
            return abort(403);
        }
        $request->validate([
            'name' => 'required',
            'username' => 'required|string|max:25|unique:users,username,'.$id.'|regex:/(^[A-Za-z0-9-_]+$)+/',
            'email' => 'required|email|max:255|unique:users,email,' . $id,
            'password' => 'sometimes',
            'password_confirmation' => 'sometimes|same:password',
            'roles' => 'required',
        ]);

        $user = User::findOrFail($id);
        $user->update($request->except('roles', 'password_confirmation'));
        // $plantuser = $request->input('plantbranch') ? $request->input('plantbranch') : [];

        $roles = $request->input('roles') ? $request->input('roles') : [];
        // dd($request->WERKS);
        $plantuser = PlantUser::where('users_id','=',$user->id)->update([
            'WERKS'=>$request->WERKS
        ]);
        $user->syncRoles($roles);
        // dd($user);
        if ($user) {
            return response()->json([
                'status' => 'success',
                'message' => 'User Successfully Updated'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        if (! Gate::allows('user_delete')) {
            return abort(403);
        }
        $get =  User::with('roles')
            ->where('users.id','=',$id)
            ->first();
        $user = User::findOrFail($id);
        $user->delete();
        $host = gethostname();
        $nameUser = auth()->user()->name;

        $getName =  $get->name;
        activity('User - Delete')
            ->log('[WEB]'.' '.$nameUser.' '.'Has Deleted a User'.' '.$getName.' '
                .'Using'.' '.$host.' '.'Computer');
        return response()->json([
            'status' => 'success']);
    }
}
