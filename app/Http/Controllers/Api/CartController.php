<?php

namespace App\Http\Controllers\Api;

use App\KONP;
use Illuminate\Http\Request;
use App\MaterialMaster;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class CartController extends Controller
{
    public function AddToCart(Request $request, $id,$qty){
// $product = DB::table('products')->where('id',$id)->first();
	$db = DB::connection('sqlsrv2')->getDatabaseName();
		// dd($qty);
// $db = DB::table('MATERIAL_MASTER')->where('MATNR_ID',$id)->first();
	$product = KONP::select('KONP.KBETR','mm.MAKTX','mm.EAN11','mm.*')
    ->join('VCYSYS820.dbo.A801 as a8','KONP.KNUMH','=','a8.KNUMH')
	->join('VCYSYS820.dbo.MATERIAL_MASTER as mm','a8.MATNR','=','mm.MATNR')
    ->where(function ($q) {
        $q->whereDate('a8.DATAB', '<=', date('Y-m-d'))
            ->whereDate('a8.DATBI', '>=', date('Y-m-d'));
	})
	->where('mm.MATNR',$id)->first();

 //custom qty 
    // $product1 = DB::table('pos')->where('pro_id',$id)->first();
  	// $subtotal1 = $product1->pro_quantity * $product1->product_price;
	// DB::table('pos')->where('pro_id',$id)->update(['sub_total'=> $subtotal1]);
		 


    	$check = DB::table('pos')->where('pro_id',$id)->first();

    	if ($check) {
    	// DB::table('pos')->where('pro_id',$id)->increment('pro_quantity');
		DB::table('pos')->where('id', $id)->update(['pro_quantity' => $qty]);

       $product = DB::table('pos')->where('pro_id',$id)->first();
  	   $subtotal = $product->pro_quantity * $product->product_price;
  	   DB::table('pos')->where('pro_id',$id)->update(['sub_total'=> $subtotal]);

    	}else{
    	$data = array();
    	$data['pro_id'] = $id;
    	$data['pro_name'] = $product->MAKTX;
    	$data['pro_quantity'] = $qty;
    	$data['product_price'] = $product->KBETR;
		$data['sub_total'] = $product->KBETR;
		$data['pro_barcode'] = $product->EAN11;
		$data['discount'] = $product->discount;
		// dd($data['pro_quantity'] = $qty);
    	DB::table('pos')->insert($data);
    	}

		// return response()->json($data);
		// DB::table('pos')->insert($data);
 
	}
	
	public function AddToCartResume(Request $request, $id){

	$product = DB::table('hold')->where('id',$id)->first();    

    $check = DB::table('pos')->where('pro_id',$id)->first();

    	if ($check) {
    	DB::table('pos')->where('pro_id',$id)->increment('pro_quantity');

       $product = DB::table('pos')->where('pro_id',$id)->first();
  	   $subtotal = $product->pro_quantity * $product->product_price;
  	   DB::table('pos')->where('pro_id',$id)->update(['sub_total'=> $subtotal]);
		
    	}else{
    	$data = array();
    	$data['pro_id'] = $product->pro_id;
    	$data['pro_name'] = $product->pro_name;
    	$data['pro_quantity'] = $product->pro_quantity;
    	$data['product_price'] = $product->product_price;
		$data['sub_total'] = $product->sub_total;
		$data['pro_barcode'] = $product->pro_barcode;
		$data['discount'] = $product->discount;
    	DB::table('pos')->insert($data);
		}
		
		

    }


  public function CartProduct(){
   $cart = DB::table('pos')->get();
  	return response()->json($cart);
  }



 public function removeCart($id){
 	DB::table('pos')->where('id',$id)->delete();
 	return response('Done');

 }


  public function increment($id){
  	$quantity = DB::table('pos')->where('id',$id)->increment('pro_quantity');

  	$product = DB::table('pos')->where('id',$id)->first();
  	$subtotal = $product->pro_quantity * $product->product_price;
  	DB::table('pos')->where('id',$id)->update(['sub_total'=> $subtotal]);
  	return response('Done');
  }
  
 
  public function incrementAdd($id, $qty){
	//   dd($qty);
  	$quantity = DB::table('pos')->where('id', $id)->update(['pro_quantity' => $qty]);
  	$product = DB::table('pos')->where('id',$id)->first();
	  $subtotal = $product->pro_quantity * $product->product_price;
	//   $subtotal = $proquantity;
  	DB::table('pos')->where('id',$id)->update(['sub_total'=> $subtotal]);
  	return response('Done');
  }
public function cartPriceAdd($id, $price){
	//   dd($qty);
	  $quantity = DB::table('pos')->where('id', $id)->update(['product_price' => $price]);
	  $product = DB::table('pos')->where('id',$id)->first();
	  $subtotal = $product->pro_quantity * $product->product_price;
	//   $subtotal = $proquantity;
  	DB::table('pos')->where('id',$id)->update(['sub_total'=> $subtotal]);
 
  	return response('Done');
  }
  
  public function discountCart($id, $disc){
	//   dd($qty);
	$n = $disc / 100;
	$quantity = DB::table('pos')->where('id', $id)->update(['discount' => $n]);
	  $product = DB::table('pos')->where('id',$id)->first();
	
	  $formatDiscount = $n / 100;

      $subtotal1 = $product->pro_quantity * $product->product_price;
      $subtotal =  $subtotal1 - ($subtotal1 * (float)$formatDiscount);
      DB::table('pos')->where('id',$id)->update(['sub_total'=> $subtotal]);
      
    return response('Done');
  }


  public function decrement($id){
	  $quantity = DB::table('pos')->where('id',$id)->decrement('pro_quantity');
	  $product = DB::table('pos')->where('id',$id)->first();
	// $quan =  $product->pro_quantity 
  	
  	$subtotal = $product->pro_quantity * $product->product_price;
  	DB::table('pos')->where('id',$id)->update(['sub_total'=> $subtotal]);
  	return response('Done');
  }


  public function Vats(){
  	$vat = DB::table('vat')->first();
  	return response()->json($vat);
  }


}
