<?php

namespace App\Http\Controllers\Api;

use App\Hold;
use DateTime;
use App\MaterialMaster;
use charlieuki\ReceiptPrinter\ReceiptPrinter as ReceiptPrinter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class PosController extends Controller
{
   public function GetProduct($id){

   	// $product = DB::table('products')
   	// 	->where('category_id',$id)
   	// 	->get();
   	// 	return response()->json($product);
       $material = MaterialMaster::select('MATERIAL_MASTER.*')
       ->where('EAN11',$id)
       ->get();
       return response()->json($material);
   } 

   public function OrderDone(Request $request){

   	 $validatedData = $request->validate([
      'customer_id' => 'required',
      'payby' => 'required',
      'pay' => 'required',
   	 ]);
   
    $data = array();
    $data['customer_id'] = $request->customer_id;
    $data['qty'] = $request->qty1;
    $data['sub_total'] = $request->subtotal;
    $data['vat'] = $request->vat;
    $data['total'] = $request->total;
    $data['pay'] = $request->pay;
    $data['payby'] = $request->payby;
    $data['discount'] = $request->discTotal;
    $data['order_date'] = date('d/m/Y');
    $data['order_month'] = date('F');
    $data['order_year'] = date('Y');
    $order_id = DB::table('orders')->insertGetId($data);

    $contents = DB::table('pos')->get();

    $odata = array();
    foreach ($contents as $content) {
    $odata['order_id'] = $order_id;
    $odata['product_id'] = $content->pro_id;
    $odata['pro_quantity'] = $content->pro_quantity;
    $odata['product_price'] = $content->product_price;
    $odata['sub_total'] = $content->sub_total;
    $odata['pro_name'] =$content->pro_name;
    $odata['pro_barcode'] = $content->pro_barcode;
    DB::table('order_details')->insert($odata); 

       
        // DB::table('products')
        // 	->where('id',$content->pro_id)
        // 	->update(['product_quantity' => DB::raw('product_quantity -' .$content->pro_quantity)]);

    }
    DB::table('pos')->delete();
    return response('Done'); 

   }
   public function OrderHold(Request $request){

   	 $validatedData = $request->validate([
      // 'customer_id' => 'required',
      // 'payby' => 'required',
      // 'pay' => 'required',
   	 ]);
      // $data = array();
      // $data['customer_id'] = $request->customer_id;
      // $data['qty'] = $request->qty1;
      // $data['sub_total'] = $request->subtotal;
      // $data['vat'] = $request->vat;
      // $data['total'] = $request->total;
      // $data['pay'] = $request->pay;
      // $data['payby'] = $request->payby;
      // $data['discount'] = $request->discTotal;
      // $data['order_date'] = date('d/m/Y');
      // $data['order_month'] = date('F');
      // $data['order_year'] = date('Y');
      // $order_id = DB::table('orders')->insertGetId($data);
 

    $contents = DB::table('pos')->get();

    $odata = array();

    $array_id = [];
    foreach ($contents as $key=>$content) {
    $odata['pro_id'] = $content->pro_id;
    $odata['pro_quantity'] = $content->pro_quantity;
    $odata['product_price'] = $content->product_price;
    $odata['sub_total'] = $content->sub_total;
    $odata['pro_name'] =$content->pro_name;
    $odata['pro_barcode'] = $content->pro_barcode;
    $odata['discount'] = $content->discount;
    // $query = DB::table('hold')->insert($odata); 
    $query = Hold::create($odata); 
    $array_id[$key] =  $query->id;
       
        // DB::table('products')
        // 	->where('id',$content->pro_id)
        // 	->update(['product_quantity' => DB::raw('product_quantity -' .$content->pro_quantity)]);
    }
    // return response()->json($contents);
    DB::table('pos')->delete();
  
    return response()->json($array_id); 

   }
 

  public function SearchOrderDate(Request $request){
    $orderdate = $request->date;
    $newdate = new DateTime($orderdate);
    $done = $newdate->format('d/m/Y'); 

    $order = DB::table('orders')
        ->select('orders.*')
        ->where('orders.order_date',$done)
        ->get();

    return response()->json($order);

  }



   public function TodaySell(){
     $date = date('d/m/Y');
     $sell = DB::table('orders')->where('order_date',$date)->sum('total');
     return response()->json($sell);
   }

   public function TodayIncome(){
     $date = date('d/m/Y');
     $income = DB::table('orders')->where('order_date',$date)->sum('pay');
     return response()->json($income);
   }

  //   public function TodayDue(){
  //    $date = date('d/m/Y');
  //    $todaydue = DB::table('orders')->where('order_date',$date)->sum('due');
  //    return response()->json($todaydue);
  //  }


  //  public function TodayExpense(){
  //   $date = date('d/m/Y');
  //    $expense = DB::table('expenses')->where('expense_date',$date)->sum('amount');
  //    return response()->json($expense);
  //  }

//  public function Stockout(){

//   $product = DB::table('products')->where('product_quantity','<','1')->get();
//   return response()->json($product);

//  }


}
