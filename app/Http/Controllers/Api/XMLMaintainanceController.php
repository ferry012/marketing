<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\xml_exports;
use Illuminate\Http\Request;
use DB;

class XMLMaintainanceController extends Controller
{

    public function index()
    {
            $xml_data = xml_exports::all();
            return response()->json($xml_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $host = gethostname();
        $nameUser = auth()->user()->name;

        $data = array();
        $data['WERKS'] = $request->WERKS;
//        $data['PLANT'] = $request->plant;
        $data['cost_center'] = $request->costing_center;
        $data['gl_account'] = $request->gl_account;


        $query = xml_exports::create($data);
        activity('GL Account - Create')
            ->log('[WEB]'.' '.$nameUser.' '.'Has Created a cost center:'.' '.$request->costing_center.' and'.' '.
                'gl accounts:'.' '.$request->gl_account.' '.'To Plant'.' '.$request->WERKS.' '
                .'Using'.' '.$host.' '.'Computer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $xml_data = DB::table('xml_exports')->where('id',$id)->first();
       return response()->json($xml_data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = array();
        $data['WERKS'] = $request->WERKS;
//        $data['PLANT'] = $request->PLANT;
        $data['cost_center'] = $request->cost_center;
        $data['gl_account'] = $request->gl_account;

        $gl_account = xml_exports::where('gl_account', '=', $request->gl_account)->exists();
        $cost_center = xml_exports::where('cost_center', '=', $request->gl_account)->exists();
        $name = auth()->user()->name;
        $host = gethostname();
        if (!$gl_account === true) {
            activity('GL Account - Update')
                ->log('[WEB]' .' '. $name .' '.'Has Updated'.' '.'GL Account of'.' '.$request->gl_account.' '
                    .'To'. ' ' .$request->WERKS.' '. 'Using' . ' ' . $host . ' ' . 'Computer');
        }
        else if (!$cost_center === true) {
            activity('Cost Center - Update')
                ->log('[WEB]' .' '. $name .' '.'Has Updated'.' '.'Cost Center of'.' '.$request->cost_center.' '
                    .'To'. ' ' .$request->WERKS.' '. 'Using' . ' ' . $host . ' ' . 'Computer');
        }
        $query = xml_exports::where('id',$id)->update($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $name = auth()->user()->name;
        $host = gethostname();
//        $query = DB::table('xml_exports')->where('id',$id)->delete();
        $get = xml_exports::where('id',$id)->first();
        $getVal = $get->cost_center;
        $query = xml_exports::where('id',$id)->delete();

        activity('Cost Center - Delete')
            ->log('[WEB]' . ' ' . $name . ' ' . 'Has Deleted a Cost Center of' . ' ' . '[' . $getVal . ']' . ' ' . 'Using' . ' ' . $host . ' ' . 'Computer');
    }
}
