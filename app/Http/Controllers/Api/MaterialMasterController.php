<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\MaterialMaster;
use App\Model\Inventory;
use App\Model\Plantbranches;
use Illuminate\Http\Request;

class MaterialMasterController extends Controller
{
    public function index()
    {
//        $master = MaterialMaster::select('inv.WERKS', 'MATERIAL_MASTER.MATNR', 'MATERIAL_MASTER.MAKTX')
//            ->join('INVENTORY as inv', 'MATERIAL_MASTER.MATNR', '=', 'inv.MATNR')
//            ->where('MATERIAL_MASTER.MTART', 'LIKE', '%ZPRO%')
//            ->groupBy('inv.WERKS', 'MATERIAL_MASTER.MATNR','MATERIAL_MASTER.MAKTX')
//            ->get();

        $master = MaterialMaster::select('MATERIAL_MASTER.MATNR', 'MATERIAL_MASTER.MAKTX')
            ->where('MATERIAL_MASTER.MTART', 'LIKE', '%ZPRO%')
            ->groupBy('MATERIAL_MASTER.MATNR', 'MATERIAL_MASTER.MAKTX')
            ->get();

//        $master = Inventory::select('INVENTORY.MATNR', 'mm.MAKTX')
//            ->join('MATERIAL_MASTER as mm','INVENTORY.MATNR','=','mm.MATNR')
//            ->where('mm.MTART', 'LIKE', '%ZPRO%')
//            ->groupBy('INVENTORY.MATNR','mm.MAKTX')
//            ->get();
//        $master = Plantbranches::select('mm.MATNR', 'mm.MAKTX','PLANT_BRANCHES.WERKS','PLANT_BRANCHES.NAME1')
//            ->join('INVENTORY as inv','PLANT_BRANCHES.WERKS','=','inv.WERKS')
//            ->join('MATERIAL_MASTER as mm','inv.MATNR','=','mm.MATNR')
//            ->where('mm.MTART', 'LIKE', '%ZPRO%')
//            ->groupBy('mm.MATNR','mm.MAKTX','PLANT_BRANCHES.WERKS','PLANT_BRANCHES.NAME1')
//            ->get();

//        $master = MaterialMaster::all();
        return response()->json($master);

    }
}
