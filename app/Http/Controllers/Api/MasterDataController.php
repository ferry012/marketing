<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\MaterialMaster;
use Illuminate\Http\Request;

class MasterDataController extends Controller
{
    public function index()
    {
        $product = MaterialMaster::select('inv.LABST','MATERIAL_MASTER.MATNR','mb.VERPR','MATERIAL_MASTER.MAKTX')
        ->join('INVENTORY as inv','MATERIAL_MASTER.MATNR','=','inv.MATNR')
        ->join('MBEW as mb','inv.MATNR','=','mb.MATNR')
        ->where('MATERIAL_MASTER.MTART','LIKE','%ZPRO%')
        // ->value('MATERIAL_MASTER.MTART','==','ZPRO')
        ->groupBy('inv.LABST','MATERIAL_MASTER.MATNR','mb.VERPR','MATERIAL_MASTER.MAKTX')
        // ->orderBy('MATNR_ID','asc')
        ->get();

        return response()->json($product);
    }

}
