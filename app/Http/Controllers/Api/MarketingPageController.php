<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\MarketingCollection;
use App\Model\Marketing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MarketingPageController extends Controller
{
    public function index(Request $request)
    {
//        return Marketing::latest()
//            ->paginate(5);
        if($request->showAll) {
            $query = Marketing::orderBy('plantbranch_id','asc')->get();
        }else{
            $searchValue = $request->search;
            $orderBy = $request->sortby;
            $orderByDir = $request->sortdir;
            $perPage = $request->currentpage;

            $query = Marketing::where('plantbranch_id', 'LIKE', "%$searchValue%")
                ->orderBy($orderBy, $orderByDir)->paginate($perPage);
        }
        return new MarketingCollection($query);
    }

    public function searchMarketing(Request $request) //search
    {

        if ($search = $request->search) {
            $users = Marketing::where(function ($query) use ($search) {
                $query->where('plantbranch_id', 'LIKE', "%$search%");

            })->paginate(5);
        } else {
            $users = Marketing::latest()
                ->paginate(5);

        }
        return response()->json($users);


    }

}
