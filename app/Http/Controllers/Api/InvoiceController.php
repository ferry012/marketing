<?php

namespace App\Http\Controllers\Api;

use App\InvoiceSapMessages;
use App\MaterialMaster;
use App\Model\Inventory;
use App\Model\MBEW;
use App\Plant;
use App\PlantUser;
use App\Model\Invoice;
use App\xml_exports;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Model\MarketingInventory;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\Types\Integer;
use SimpleXMLElement;
use Spatie\Activitylog\Models\Activity;
use Spatie\ArrayToXml\ArrayToXml;

class InvoiceController extends Controller
{
    public function __construct()
    {
        $environment = '';
        if (App::environment('local', 'dev')) {
            $environment = 'MAK_DEV';
        }
        if (App::environment('staging', 'production')) {
            $environment = 'MAK_PROD';
        }
        $this->environment = $environment;
    }

    public function index()
    {
        // $invoice = Invoice::all();
        // return response()->json($invoice);
        $user = auth()->user();
        $plant = PlantUser::where('users_id', '=', $user->id)->first();
        $invoice = Invoice::select('invoices.*')
            ->where('invoice_plant', '=', $plant->WERKS)
            ->get();
        return response()->json($invoice);

    }


    public function store(Request $request)
    {
//        dd(auth()->user()->name);

//        dd($nameCashier);
//        dd($request->redem_MAKTX);

        $validatedData = $request->validate([ // variable from $request
            'invoice_number' => ['required', 'unique:invoices'],
            'total_amount' => ['required'],
            'redem_item' => ['required'],
            'qty' => ['required'],
            'redem_MATNR' => ['required'],
        ]);
        $userCashier = auth()->user();
        $nameCashier = $userCashier->name;
        $data = array();
        $user = auth()->user();
        $plant = PlantUser::where('users_id', '=', $user->id)->first();
        $plant = $plant->WERKS;
//        dd($plant);
        $data['invoice_number'] = $request->invoice_number;
        $data['total_amount'] = $request->total_amount;
        $data['invoice_market_MAKTX'] = $request->redem_MAKTX;
        $data['qty'] = $request->qty;
        $data['invoice_plant'] = $plant;
        $data['invoice_MATNR'] = $request->redem_MATNR;
        $data['invoice_date'] = date('d/m/Y');
        $data['invoice_month'] = date('F');
        $data['invoice_year'] = date('Y');
        $data['cashier_name'] = $nameCashier;
//        $check = Invoice::create($data);
        $user = auth()->user();
        $plant = PlantUser::where('users_id', '=', $user->id)->first();


        $marketingInv = Inventory::where('INVENTORY.MATNR', '=', $request->redem_MATNR)
            ->where('INVENTORY.WERKS', '=', $plant->WERKS)
            ->first();


        $marketingInv->LABST = $marketingInv->LABST - $request->qty;


        if ($marketingInv->LABST < 0) {

            InvoiceSapMessages::create([
                'invoice_number' => $request->invoice_number
            ]);

            return response()->json([

                'status' => 'warning',
                'title' => 'Transaction is Invalid',
                'message' => 'The Redeemed item you selected is currently out of stocks'
            ]);
        } else {

            InvoiceSapMessages::create([
                'invoice_number' => $request->invoice_number
            ]);
            $user1 = auth()->user();
            $plant1 = PlantUser::where('users_id', '=', $user1->id)->first();
            $plant2 = $plant1->WERKS;

            $plant_name = Plant::join('VCYMAK.dbo.plantbranch as pb', 'PLANT_BRANCHES.WERKS', '=', 'pb.WERKS')
                ->where('PLANT_BRANCHES.WERKS', '=', $plant2)
                ->select('PLANT_BRANCHES.NAME1')
                ->first();
            $plant_name1 = $plant_name->NAME1;

            //TODO activity log ang filtering on monday
//            $activity = Activity::all()->last();
//            $activity->description;
//            $activity->subject;
            $name = auth()->user()->name;
            $check = Invoice::create($data);
            activity('Invoice - Create')
                ->log('[WEB]' . $name . ' ' . 'Has Redeemed' . ' ' . $request->redem_MAKTX . ' ' . 'At' . ' ' . $plant_name1);
            $activity = Activity::all()->last();

            $marketingInv->save();

            $marketingInvCost = MBEW::where('MBEW.MATNR', '=', $request->redem_MATNR)
                ->where('MBEW.BWKEY', '=', $plant->WERKS)
                ->first();
            $marketingInvCostTotal = Invoice::where('invoices.invoice_MATNR', '=', $request->redem_MATNR)
                ->where('invoices.invoice_plant', '=', $plant->WERKS)
                ->latest()
                ->first();

            $costing = $marketingInvCost->VERPR / $request->total_amount * 100;

            $marketingInvCost->VERPR = round($costing, 2);

            $marketingInvCostTotal->cost_invoice_amount = $marketingInvCost->VERPR;


            $marketingInvCostTotal->save();
            $db = \Illuminate\Support\Facades\DB::connection('sqlsrv2')->getDatabaseName();
//            $db = DB::connection('sqlsrv2')->getDatabaseName();
            $query = INVENTORY::select('mm.MEINS')
                ->join($db . '.dbo.MATERIAL_MASTER as mm', 'INVENTORY.MATNR', '=', 'mm.MATNR')
                ->where('mm.MATNR', '=', $request->redem_MATNR)
                ->first();

            $queryCostCenter = xml_exports::select('xml_exports.cost_center')
                ->join($db . '.dbo.INVENTORY as inv', 'xml_exports.WERKS', '=', 'inv.WERKS')
                ->where('inv.WERKS', '=', $plant->WERKS)
                ->first();

            $array = [
                'BRANCH' => $plant->WERKS,
                'INVOICE' => $request->invoice_number,
                'MATNR' => $request->redem_MATNR,
                'UNIT' => $query->MEINS,
                'QUANTITY' => $request->qty,
                'COSTCENTER' => $queryCostCenter->cost_center,
            ];

            $xml = ArrayToXml::convert($array, 'DATA');
            $unix = round(microtime(true) * 1000);
//            $filename = Carbon::now()->format('Ymd').'_'.Carbon::now()->format('hms').'.xml';
            $filename = $unix . '.xml';
            $path = $this->environment . '/INVOICE/' . $filename;
            $filepath = preg_replace("/\s+/", "", $path);
            $result = Storage::disk('ftp')->put($filepath, $xml);
            return $result;
        }


    }

    public function InvoiceSub($id)
    {

        $market = DB::table('marketing_inventory')->where('id', $id)->first();
        $invoice = DB::table('invoices')->where('id', $id)->first();
        $currentInv = $market->marketing_inv - $invoice->invoice_market_inventory;
        //   $subtotal = $proquantity;
        DB::table('marketing_inventory')->where('id', $id)->update(['marketing_inventory' => $currentInv]);
        return response('Done');

    }

    public function show($id)
    {
        // dd($id);
        $marketing = DB::table('invoices')->where('id', $id)->first();
        // dd($marketing);
        return response()->json($marketing);
    }


    public function showRedeemedInvoices(Request $request)
    {
        $searchValue = $request->search;
        $orderBy = $request->sortby;
        $orderByDir = $request->sortdir;
        $perPage = $request->currentpage;

        $query = InvoiceSapMessages::
        where('sap_message', '!=', null)
            ->when($searchValue !== 'null', function ($query) use ($searchValue) {
                $query->where('invoice_number', 'LIKE', '%' . $searchValue . '%');
            })
            ->orderBy($orderBy, $orderByDir)
            ->paginate($perPage);

        return response()->json($query);
    }

    public function manualExport(Request $request)
    {

        $db = \Illuminate\Support\Facades\DB::connection('sqlsrv2')->getDatabaseName();
        $invoice = Invoice::where('invoice_number', '=', $request->invoice)->first();

        $material = INVENTORY::select('mm.MEINS')
            ->join($db . '.dbo.MATERIAL_MASTER as mm', 'INVENTORY.MATNR', '=', 'mm.MATNR')
            ->where('mm.MATNR', '=', $invoice->invoice_MATNR)
            ->first();

        $queryCostCenter = xml_exports::select('xml_exports.cost_center')
            ->join($db . '.dbo.INVENTORY as inv', 'xml_exports.WERKS', '=', 'inv.WERKS')
            ->where('inv.WERKS', '=', $invoice->invoice_plant)
            ->first();

        $array = [
            'BRANCH' => $invoice->invoice_plant,
            'INVOICE' => $request->invoice,
            'MATNR' => $invoice->invoice_MATNR,
            'UNIT' => $material->MEINS,
            'QUANTITY' => $invoice->qty,
            'COSTCENTER' => $queryCostCenter->cost_center,
        ];

        //clear sap message
        $item = InvoiceSapMessages::where('invoice_number', '=', $request->invoice)->first();
        $item->sap_message = null;
        $item->save();

        $xml = new SimpleXMLElement('<XMLMAK/>');

        foreach ($array as $lineitem => $value) {
            $xml->addChild($lineitem, $value);
        }
        $xml_output = $xml->asXML();
        $unix = round(microtime(true) * 1000);
        $file_name = $unix . ".xml";
        $path = env('FTP_ROOT') . "INVOICE/" . $file_name;
        Storage::disk('ftp')->put($path, $xml_output);
    }

}
