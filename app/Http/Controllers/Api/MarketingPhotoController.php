<?php

namespace App\Http\Controllers\Api;

use App\PlantUser;
use App\Model\Marketing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MarketingPhotoController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        // dd($user->id);
        $plant = PlantUser::where('users_id','=',$user->id)->first();
        $marketing = Marketing::select('marketing.*')
        ->where('plantbranch_id','=',$plant->WERKS)
        ->get();
        return response()->json($marketing);
    }
}
