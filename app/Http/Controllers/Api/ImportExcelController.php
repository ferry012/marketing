<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Imports\CheckExcelImport;
use App\Imports\ExcelImport;
use App\Imports\UsersImport;
use App\Model\MarketingInventory;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ImportExcelController extends Controller
{

    public function importExcel(Request $request)
    {

        $validate = $request->validate([
            'import_file' => 'required|file|mimes:xls,xlsx'
        ]);
        $path = $request->file('import_file');
        $import = new ExcelImport;
        $data = Excel::import($import, $path);
        return response()->json(['message' => 'uploaded successfully'], 200);


    }

    public function checkImportExcel(Request $request)
    {
//dd($request);
        $file_handle = fopen($request->file('import_file')->getPathname(), 'r');
//        dd($file_handle);
        while (!feof($file_handle)) {
            $array[] = fgetcsv($file_handle, 0, ',');

        }
        fclose($file_handle);

        $arrReport = [];
//        https://stackoverflow.com/questions/51823278/laravel-csv-validations
        foreach ($array as $key => $value) {

            if ($key > 0 && $value !== false) {

                if (MarketingInventory::where('marketing_MATNR', '=', $value[0])
                    ->where('marketing_plantbranch_WERKS', '=', $value[3])->exists()) {
                    $arrReport[] = [
                        'marketing_MATNR' => $value[0],
                        'marketing_plantbranch_WERKS' => $value[3],
                        'index' => $key,
                    ];


                }


            }


        }


        if ($arrReport != null) {
//            dd('test');
            return response()->json($arrReport);
        } else {
//            dd('test123');
            return response()->json([

                'status' => 'success',
                'message' => 'No Duplicate Data Found, You can now proceed and make sure to convert it back to ".xlsx" Format'
            ]);

        }

        //
    }

}
