<?php

namespace App\Http\Controllers\Api;

use App\Exports\ExcelExport;
use App\Exports\PDFExport;
use App\Http\Controllers\Controller;
use DateTime;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class PDFExportController extends Controller
{
    public function pdfExport(Request $request)
    {

//        $orderdate = $request->date;
//        $newdate = new DateTime($orderdate);
//        $done = $newdate->format('d/m/Y');

        $date_from = $request->date_from;
        $newdate_from = new DateTime($date_from);
        $from = $newdate_from->format('Y/m/d');
        $date_to = $request->date_to;
        $newdate_to = new DateTime($date_to);
        $to = $newdate_to->format('Y/m/d');
        $host = gethostname();
        $nameUser = auth()->user()->name;

//        https://stackoverflow.com/questions/57153985/how-to-pass-parameter-to-export-file-of-laravel-excel-for-db-query
//        https://codepen.io/nigamshirish/pen/ZMpvRa
//        https://laravel-vuejs.com/export-data-to-excel-in-laravel-using-maatwebsite/
//        https://github.com/Maatwebsite/Laravel-Excel/issues/2651
//        https://laracasts.com/discuss/channels/general-discussion/generate-excel-based-on-the-query-from-database-using-vue-and-laravel-maatwebsite-package

        activity('PDF - Export')
            ->log('[WEB]'.' '.$nameUser.' '.'Has Exported a PDF Report From'.' '.$from.' '.'and To'.' '.$to.' '
                .'Using'.' '.$host.' '.'Computer');
        return Excel::download(new PDFExport($from,$to),'TRIUMPH HOME DEPOT REPORTS.pdf', \Maatwebsite\Excel\Excel::DOMPDF);
    }
}
