<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use App\ReservationDetails;
use App\ReservationHeader;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $id = Auth::user()->id;
//        $query = ReservationHeader::create(['res_no'=>'1','customer_id'=>'1','cashier_id'=>$id,'payment_id'=>'1','total' => '100','status'=>'S']);
//        return $query;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


//
//            dd($request->customer['customer_id']);
//            $data = $request->validate([
//                "*.product"     => "required|array",
//                "salesman"      => "required|array",
//            ]);
//
//            dd($data);


            $res_no = ReservationHeader::max('res_no');
            $reserve_number = $res_no + 1;
            $res_header = new ReservationHeader;

            if($request->status == 'F'){
                $res_header->customer_id = $request->customer['customer_id'];
                $res_header->payment_id = $request->payment[0]['payment_id'];
            }else{
                $res_header->customer_id = '0001009910';
                $res_header->payment_id = '0';
            }

            $res_header->res_no =$reserve_number;
            $res_header->cashier_id = 1;
            $res_header->salesman_id = $request->salesman['salesman_id'];
            $res_header->total = $request->total;
            $res_header->status = $request->status;
            $res_header->overall_discount = $request->discount;
            $res_header->save();

            if($res_header){
                foreach ($request->products as $product){
                    $res_details = new ReservationDetails;
                    $res_details->ref_no = $reserve_number;
                    $res_details->product_id = $product['EAN11'];
                    $res_details->price = $product['price'];
                    $res_details->qty = $product['qty'];
                    $res_details->discount = $product['discount'];
                    $res_details->save();
                }

                return response()->json([
                    'reserve_number' => $reserve_number,
                    'created_date' => 'User successfully created'
                ]);

//                return $reserve_number;
            }


//        dd(count($request->products ));

//        ReservationHeader::create($data->all());

//       $data = json_encode($request->products);
//            dd($data);
//        $request->validate([
//            'total'=> 'required'
//        ]);
//
//        ReservationHeader::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
