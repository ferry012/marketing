<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $showAll = $request->get('showAll');
        $searchCode = $request->get('code');

        if($showAll == '1'){
            $product = Product::join('A801 as a','a.MATNR','MATERIAL_MASTER.MATNR')
                ->join('KONP as k','k.KNUMH','a.KNUMH')
                ->where('MAKTX','LIKE',"%$searchCode%")
                ->orwhere('EAN11','LIKE',"%$searchCode%")
                ->select('k.KBETR','MATERIAL_MASTER.*')
                ->orderBy('MAKTX','asc')->get();
        }else{
            $product = Product::join('A801 as a','a.MATNR','MATERIAL_MASTER.MATNR')
                ->join('KONP as k','k.KNUMH','a.KNUMH')
                ->where('MAKTX',"$searchCode")
                ->orwhere('EAN11',"$searchCode")
                ->select('k.KBETR','MATERIAL_MASTER.*')
                ->orderBy('MAKTX','asc')->get();
        }

        return response()->json($product);
    }
}
