<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['res','cashier_id','salesman_id','customer_id','payment_id','amount'];

}
