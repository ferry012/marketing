<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table='CUSTOMER_MASTER';
    protected $connection ='sqlsrv2';
    protected $fillable = [
        'KUNNR', 'NAME1'
    ];
}
