<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Plantbranches extends Model
{
    protected $table='PLANT_BRANCHES';
    protected $connection ='sqlsrv2';
}
