<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class check_excel extends Model
{
    protected $table = 'check_excel';

    protected $fillable = [
        'marketing_MATNR',
        'marketing_MAKTX',
        'marketing_inv',
        'marketing_total_amount',
        'marketing_plantbranch_WERKS',
        'cost',
        'market_date',
        'market_month',
        'market_year',
    ];
}
