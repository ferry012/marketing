<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MarketingInventory extends Model
{
    protected $table = 'marketing_inventory';

    protected $fillable = [
        'marketing_MATNR',
        'marketing_MAKTX',
        'marketing_total_amount',
        'marketing_plantbranch_WERKS',
        'market_date',
        'market_month',
        'market_year',
        'valid_from',
        'valid_to',

    ];
}
