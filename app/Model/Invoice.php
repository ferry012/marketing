<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Contracts\Activity;

class Invoice extends Model
{
//    use LogsActivity;

    protected $table = 'invoices';
    protected $fillable = [
        'invoice_number',
        'total_amount',
        'invoice_market_MAKTX',
        'qty',
        'invoice_plant',
        'invoice_MATNR',
        'invoice_date',
        'invoice_month',
        'invoice_year',
        'cost_invoice_amount',
        'cashier_name',

    ];

//    protected $guarded =[  'invoice_number',
//        'total_amount',
//        'invoice_market_MAKTX',
//        'qty',
//        'invoice_plant',
//        'invoice_MATNR',
//        'invoice_date',
//        'invoice_month',
//        'invoice_year',
//        'cost_invoice_amount',
//        'cashier_name',];

//protected static $logAttributes = [ 'invoice_number',
//    'total_amount',
//    'invoice_market_MAKTX',
//    'qty',
//    'invoice_plant',
//    'invoice_MATNR',
//    'invoice_date',
//    'invoice_month',
//    'invoice_year',
//    'cost_invoice_amount',
//    'cashier_name',];

//    protected $casts = [
//        'created_at' => 'datetime:Y-m-d',
//        'updated_at'=>'datetime:m/d/Y h:i s'
//    ];
//    public function tapActivity(Activity $activity, string $eventName)
//    {
//        $activity->description = "activity.logs.message.{$eventName}";
//    }
//
//    protected static $logName = 'Invoice';
//    protected static $recordEvents = ['created'];
}
