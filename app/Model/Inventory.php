<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Inventory extends Model
{


    protected $table='INVENTORY';
    protected $connection ='sqlsrv2';
     protected $fillable = [
         'LABST','MATNR','WERKS',
     ];
    protected $primaryKey = 'MATNR_ID';
    public $timestamps = false;
}
