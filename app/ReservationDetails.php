<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservationDetails extends Model
{
    protected $table = 'reservation_details';
    protected $fillable = ['res_no','product_id','price','qty','discount'];
}
