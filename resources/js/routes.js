/*auth*/
// import Home from './components/Home'
import Login from './components/authentication/Login'
import Profile from "./components/profile/Profile";

/*users*/
import Users from './components/user/Users'
import UsersTable from "./components/user/UsersTable";
import UserCreate from "./components/user/Create";
import UserView from "./components/user/Show";
import UserEdit from "./components/user/Edit";


/*permission*/
import Permission from "./components/permission/Permission";
import PermissionsTable from "./components/permission/PermissionsTable";
import PermissionCreate from "./components/permission/Create";
import PermissionView from "./components/permission/Show";
import PermissionEdit from "./components/permission/Edit";

/*role*/
import Role from "./components/role/Role";
import RolesTable from "./components/role/RolesTable";
import RoleCreate from "./components/role/Create";
import RoleView from "./components/role/Show";
import RoleEdit from "./components/role/Edit";

//Point of sale

import pos from "./components/pointofsale/pos";

/**Marketing */
import createMarket from "./components/marketing/create.vue";
import editMarket from "./components/marketing/edit.vue";
import indexMarket from "./components/marketing/index.vue";
import Market from "./components/marketing/markets.vue";

/**Marketing Inventory */
import createMarketInv from "./components/marketing_inventory/create.vue";
import editMarketInv from "./components/marketing_inventory/edit.vue";
import indexMarketInv from "./components/marketing_inventory/index.vue";
import checkMarketInv from "./components/marketing_inventory/check.vue";
import MarketInv from "./components/marketing_inventory/marketing_inv.vue";


/** Marketing Reports */
import marketingReportIndex from "./components/marketing_report/search.vue";
import marketingReportParent from "./components/marketing_report/report_parent.vue";

//Invoice
import invoiceCreate from "./components/invoice/create.vue";
import invoiceIndex from "./components/invoice/index.vue";
import inVoice from "./components/invoice/invoice.vue";
import redeemedInvoices from "./components/invoice/redeemed_invoices";
import invoicePromo from "./components/invoice/promo.vue";

// let createMarket = require('./components/marketing/create.vue').default;
// let editMarket = require('./components/marketing/edit.vue').default;
// let indexMarket =require('./components/marketing/search.vue').default;


/*Master Data*/

/*Payment*/
import Payment from "./components/payment/Payment";
import PaymentsTable from "./components/payment/PaymentsTable";
import PaymentCreate from "./components/payment/Create";
import PaymentEdit from "./components/payment/Edit";

/*Reservation*/
import Reservation from "./components/reservation/Reservation";
import ReservationsTable from "./components/reservation/ReservationsTable";
import ReservationCreate from "./components/reservation/Create";
import ReservationView from "./components/reservation/Show";
import ReservationEdit from "./components/reservation/Edit";

/*Reason*/
import Reason from "./components/reason/Reason";
import ReasonsTable from "./components/reason/ReasonsTable";
import ReasonCreate from "./components/reason/Create";
import ReasonEdit from "./components/reason/Edit";
import ReasonShow from "./components/reason/Show";

/**/
import Plant from "./components/plant/Plant";
import PlantsTable from "./components/plant/PlantsTable";

/*passport*/
import Passport from "./components/passport/Passport";

/*errors*/
import Error_404 from "./components/errors/404"

//Reservation
import ReservationTrans from "./components/reservationTrans/index";

//XML_Exports
import xmlEdit from "./components/xml_exports/edit";
import xmlCreate from "./components/xml_exports/create";
import xmlIndex from "./components/xml_exports/index";
import xmlParent from "./components/xml_exports/xml_parent";

//Master Invoice
import masterIndex from "./components/master_invoice/index";
import masterEdit from "./components/master_invoice/edit";
import masterParent from "./components/master_invoice/master";

export const routes = [
    // {
    //     path: '/',
    //     name: 'home',
    //     component: Home,
    //     meta: {
    //         title: 'Home',
    //         requiresAuth: true,
    //     }
    // },
    {
        path: '/',
        name: 'invoice',
        component: invoiceIndex,
        meta: {
            title: 'Home',
            requiresAuth: true,
        }
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            title: 'Login',

        }
    },
    {
        path: '/profile',
        name: 'profile',
        component: Profile,
        meta: {
            title: 'Profile',
            requiresAuth: true,
        }
    },
    {
        path: '/users',
        component: Users,
        children: [
            {
                path: '/',
                component: UsersTable,
                name: 'users-table',
                meta: {
                    title: 'Users',
                    roles: ['administrator','super_admin'],
                    requiresAuth: true,
                }
            },
            {
                path: 'create',
                component: UserCreate,
                name: 'users-create',
                meta: {
                    title: 'Create User',
                    roles: ['administrator','super_admin'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id',
                component: UserView,
                name: 'users-view',
                meta: {
                    title: 'View User',
                    roles: ['administrator','super_admin'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id/edit',
                component: UserEdit,
                name: 'users-edit',
                meta: {
                    title: 'Edit User',
                    roles: ['administrator','super_admin'],
                    requiresAuth: true,
                }
            }
        ]
    },
    {
        path: '/permissions',
        component: Permission,
        children: [
            {
                path: '/',
                component: PermissionsTable,
                name: 'permission-table',
                meta: {
                    title: 'Permissions',
                    roles: ['super_admin'],
                    requiresAuth: true,
                }
            },
            {
                path: 'create',
                component: PermissionCreate,
                name: 'permission-create',
                meta: {
                    title: 'Create Permission',
                    // roles: ['administrator'],
                    roles: ['super_admin'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id',
                component: PermissionView,
                name: 'permission-view',
                meta: {
                    title: 'View Permission',
                    roles: ['super_admin'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id/edit',
                component: PermissionEdit,
                name: 'permission-edit',
                meta: {
                    title: 'Edit Permission',
                    roles: ['super_admin'],
                    requiresAuth: true,
                }
            }
        ]
    },
    {
        path: '/roles',
        component: Role,
        children: [
            {
                path: '/',
                component: RolesTable,
                name: 'role-table',
                meta:

                {
                    title: 'Roles',
                    roles: ['super_admin','administrator'],
                    requiresAuth: true,
                }

            },
            {
                path: 'create',
                component: RoleCreate,
                name: 'role-create',
                meta:

                {
                    title: 'Create Role',
                    roles: ['super_admin','administrator'],
                    requiresAuth: true,
                }

            },
            {
                path: ':id',
                component: RoleView,
                name: 'role-view',
                meta: {
                    title: 'View Role',
                    roles: ['super_admin','administrator'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id/edit',
                component: RoleEdit,
                name: 'role-edit',
                meta: {
                    title: 'Edit Role',
                    roles: ['super_admin','administrator'],
                    requiresAuth: true,
                }
            }
        ]
    },
    {
        //POS
        path: '/pos',
        component: pos,
        children: [
            {
                path: '/',
                component: pos,
                name: 'pos',
                meta: {
                    title: 'Roles',
                    // roles: ['administrator','Casiher'],
                    requiresAuth: true,
                }
            },
        ]
    },
    {
        path: '/payment',
        component: Payment,
        children: [
            {
                path: '/',
                component: PaymentsTable,
                name: 'payment-table',
                meta: {
                    title: 'Payment',
                    roles: ['administrator'],
                    requiresAuth: true,
                }
            },
            {
                path: 'create',
                component: PaymentCreate,
                name: 'payment-create',
                meta: {
                    title: 'Create Payment Option',
                    roles: ['administrator'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id/edit',
                component: PaymentEdit,
                name: 'payment-edit',
                meta:
                {
                    title: 'Edit Payment Option',
                    roles: ['administrator'],
                    requiresAuth: true,
                }
            }
        ]
    },
    // { path: '/store-market', component: createMarket, name:'store-market'},
    // { path: '/market', component: indexMarket, name:'index-market'},
    // { path: '/edit-market/:id', component: editMarket, name:'edit-market'},

    /*Xml Exports*/
    {
        path: '/xml-exports',
        component: xmlParent,
        children: [
            {
                path: '/',
                component: xmlIndex,
                name: 'xml-index',
                meta: {
                    title: 'XML Data',
                    requiresAuth: true,
                }
            },
            {
                path: ':id/edit',
                component: xmlEdit,
                name: 'xml-edit',
                meta: {
                    title: 'Xml Edit',
                    requiresAuth: true,
                }
            },
            {
                path: 'create',
                component: xmlCreate,
                name: 'xml-create',
                meta: {
                    title: 'Xml Create',
                    requiresAuth: true,
                }

            }
        ]
    },
    //MASTER INVOICE
    {
        path: '/master-invoice',
        component: masterParent,
        children: [
            {
                path: '/',
                component: masterIndex,
                name: 'master-invoice-index',
                meta: {
                    title: 'Master Data Warehouse',
                    roles: ['super_admin','administrator'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id/edit',
                component: masterEdit,
                name: 'master-invoice-edit',
                meta: {
                    title: 'Marketing Edit',
                    roles: ['super_admin','administrator'],
                    requiresAuth: true,
                }
            },
        ]
    },
    {
        path: '/marketing',
        component: Market,
        children: [
            {
                path: '/',
                component: indexMarket,
                name: 'marketing-index',
                meta: {
                    title: 'Marketing',
                    requiresAuth: true,
                }
            },
            {
                path: ':id/edit',
                component: editMarket,
                name: 'marketing-edit',
                meta: {
                    title: 'Marketing Edit',
                    requiresAuth: true,
                }
            },
            {
                path: 'create',
                component: createMarket,
                name: 'marketing-create',
                meta: {
                    title: 'Marketing Create',
                    requiresAuth: true,
                }

            }
        ]
    },
    {
        //Report
        path: '/marketing-report',
        // name: 'marketing-inventory',
        component: marketingReportParent,
        children: [
            {
                path: '/',
                component: marketingReportIndex,
                name: 'marketing-report-index',
                meta: {
                    title: 'Marketing Reports',
                    requiresAuth: true,
                }
            },
        ]
    },
    {
        path: '/marketing-inventory',
        // name: 'marketing-inventory',
        component: MarketInv,
        children: [
            {
                path: '/',
                component: indexMarketInv,
                name: 'marketing-inventory-index',
                meta: {
                    title: 'Marketing Inventory',
                    requiresAuth: true,
                }
            },
            {
                path: 'create',
                component: createMarketInv,
                name: 'marketing-inventory-create',
                meta: {
                    title: 'Marketing Inventory Create',
                    requiresAuth: true,
                }

            },
            {
                path: ':id/edit',
                component: editMarketInv,
                name: 'marketing-inventory-edit',
                meta: {
                    title: 'Marketing Inventory Edit',
                    requiresAuth: true,
                }

            },
            {
                path: 'check',
                component: checkMarketInv,
                name: 'marketing-inventory-check-excel',
                meta: {
                    title: 'Marketing Inventory Upload',
                    requiresAuth: true,
                }

            },

        ]
    },

    {
        path: '/invoice',
        // name: 'invoicehome',
        component: inVoice,
        children: [
            {
                path: '/',
                component: invoiceIndex,
                name: 'invoice-index',
                meta: {
                    title: '',
                    requiresAuth: true,
                }
            },
            // {
            //     path: 'create',
            //     component: invoiceCreate,
            //     name: 'invoice-create',
            //     meta: {
            //         title: 'Invoice Create',
            //         requiresAuth: true,
            //     }
            //
            // },
            // {
            //     path: 'promo',
            //     component: invoicePromo,
            //     name: 'invoice-promo',
            //     meta: {
            //         title: 'Invoice Promo',
            //         requiresAuth: true,
            //     }
            //
            // }

            {
                path: 'redeemed',
                name: 'invoice-redeemed',
                component: redeemedInvoices,
                meta: {
                    title: 'Redeemed Invoices',
                    requiresAuth: true,
                }
            },
        ]
    },

    {
        path: '/reasons',
        component: Reason,
        children: [
            {
                path: '/',
                component: ReasonsTable,
                name: 'reason-table',
                meta: {
                    title: 'Reason',
                    roles: ['master_data_maintenance'],
                    requiresAuth: true,
                }
            },
            {
                path: 'create',
                component: ReasonCreate,
                name: 'reason-create',
                meta: {
                    title: 'Create Reason Type',
                    roles: ['administrator'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id/edit',
                component: ReasonEdit,
                name: 'reason-edit',
                meta: {
                    title: 'Edit Reason Type',
                    roles: ['administrator'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id',
                component: ReasonShow,
                name: 'reason-view',
                meta:{
                    title: 'View Reason Type',
                    roles: ['administrator'],
                    requiresAuth: true
                }
            }
        ]
    },
    {
        path: '/reservations',
        component: Reservation,
        children: [
            {
                path: '/',
                component: ReservationsTable,
                name: 'reservation-table',
                meta: {
                    title: 'Reservation',
                    roles: ['administrator'],
                    requiresAuth: true,
                }
            },
            {
                path: 'create',
                component: ReservationCreate,
                name: 'reservation-create',
                meta: {
                    title: 'Create Reservation Option',
                    roles: ['administrator'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id',
                component: ReservationView,
                name: 'reservation-view',
                meta: {
                    title: 'View Reservation Option',
                    roles: ['administrator'],
                    requiresAuth: true,
                }
            },
            {
                path: ':id/edit',
                component: ReservationEdit,
                name: 'reservation-edit',
                meta: {
                    title: 'Edit Reservation Option',
                    roles: ['administrator'],
                    requiresAuth: true,
                }
            }
        ]
    },
    {
        path: '/reservation',
        component: ReservationTrans,
        children: [
            {
                path: '/',
                component: ReservationTrans,
                name: 'reservation-index',
                meta: {
                    title: 'Reservation',
                    roles: ['administrator','Casiher'],
                    requiresAuth: true,
                }
            }
        ]
    },
    {
        path: '/plants',
        component: Plant,
        children: [
            {
                path: '/',
                component: PlantsTable,
                name: 'plant-table',
                meta: {
                    title: 'Plant',
                    roles: ['administrator'],
                    requiresAuth: true,
                }
            }
            ]
    },
    {
        path: '/passport',
        component: Passport,
        meta: {
            title: 'Passport',
            requiresAuth: true,
            roles: ['administrator'],
        }
    },
    // {
    //     path: '/404',
    //     name: 'error_404',
    //     component: Error_404,
    //     meta: {
    //         title: 'Error 404',
    //     }
    // },
    // {path: '*', redirect: '/404'},

];
