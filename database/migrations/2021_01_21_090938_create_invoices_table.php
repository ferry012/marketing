<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id('id');
            $table->integer('invoice_number');
            $table->integer('total_amount');
            $table->string('invoice_market_MAKTX');
            $table->string('invoice_MATNR');
            $table->string('invoice_plant');
            $table->integer('qty');
            $table->string('invoice_date')->nullable();
            $table->string('invoice_month')->nullable();
            $table->string('invoice_year')->nullable();
            $table->decimal('cost_invoice_amount',10,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
