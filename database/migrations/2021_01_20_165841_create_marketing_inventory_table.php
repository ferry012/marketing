<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarketingInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketing_inventory', function (Blueprint $table) {
            $table->id('id');
            $table->string('marketing_MATNR');
            $table->string('marketing_MAKTX');
            $table->string('marketing_total_amount');
            $table->string('marketing_plantbranch_WERKS');
            $table->string('market_date')->nullable();
            $table->string('market_month')->nullable();
            $table->string('market_year')->nullable();
            $table->string('valid_from')->nullable();
            $table->string('valid_to')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketing_inventory');
    }
}
