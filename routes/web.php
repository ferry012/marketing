<?php

use App\Model\Invoice;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
//
//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');

//Route::get('{path}', function () {
//    return view('home');
//})->where('path', '(.*)');

//Route::get('/test123', function () {
//    $db = DB::connection('sqlsrv2')->getDatabaseName();
//
//    $query = DB::table('invoices as a')
//        ->join($db . '.dbo.INVENTORY as b', function ($join) {
//            $join->on('b.MATNR', '=', 'a.invoice_MATNR');
//            $join->on('b.WERKS', '=', 'a.invoice_plant');
//        })
//        ->where('a.invoice_date','=','09/02/2021')
//        ->select('b.MATNR','a.invoice_market_MAKTX',
//            DB::raw("'2311_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
//		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2311)"),
//            DB::raw("'2311_total_LABST'=(SELECT SUM(LABST) FROM VCYSYS820.dbo.INVENTORY AS c
//		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2311)"),
//
//            DB::raw("'2321_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
//		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2321)"),
//            DB::raw("'2321_total_LABST'=(SELECT SUM(LABST) FROM VCYSYS820.dbo.INVENTORY AS c
//		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2321)"),
//
//            DB::raw("'2331_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
//		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2331)"),
//            DB::raw("'2331_total_LABST'=(SELECT SUM(LABST) FROM VCYSYS820.dbo.INVENTORY AS c
//		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2331)"),
//
//            DB::raw("'2341_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
//		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2341)"),
//            DB::raw("'2341_total_LABST'=(SELECT SUM(LABST) FROM VCYSYS820.dbo.INVENTORY AS c
//		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2341)"),
//
//            DB::raw("'2351_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
//		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2351)"),
//            DB::raw("'2351_total_LABST'=(SELECT SUM(LABST) FROM VCYSYS820.dbo.INVENTORY AS c
//		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2351)"),
//
//            DB::raw("'2361_total_qty'=(SELECT SUM(d.qty) FROM invoices AS d
//		            WHERE b.MATNR = d.invoice_MATNR AND d.invoice_plant = 2361)"),
//            DB::raw("'2361_total_LABST'=(SELECT SUM(LABST) FROM VCYSYS820.dbo.INVENTORY AS c
//		            WHERE b.MATNR = c.MATNR AND c.WERKS = 2361)")
//        )
//        ->groupBy('b.MATNR','a.invoice_market_MAKTX')
//        ->get()
//        ->toArray();
//
//    dd($query);
//});
//Route::get('/test123', function () {
//    $db = DB::connection('sqlsrv2')->getDatabaseName();
//
//    $created_at = Invoice::select('invoices.created_at')
//        ->where('invoices.invoice_date','=','2021/02/09')
//        ->get();
//    dd($created_at);
//});

Route::get('/{any}','AppController@index')->where('any','.*');
