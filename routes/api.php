<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('login', 'Api\Auth\AuthController@login')->name('login');
Route::post('oauth/token', 'Api\AccessTokenController@issueToken');
Route::post('refresh', 'Api\Auth\AuthController@refresh')->name('api.refresh');

Route::middleware('auth:api')->group(function () {

    Route::post('logout', 'Api\Auth\AuthController@logout')->name('logout');
    Route::post('register', 'Api\Auth\AuthController@register')->name('register');
    Route::get('auth-user', 'Api\Auth\AuthController@authenticatedUser')->name('auth-user');
    Route::patch('auth-user/update', 'Api\Auth\AuthController@authenticatedUserUpdate')->name('auth-user');
    Route::resource('users', 'Api\UserController');
    Route::resource('permissions', 'Api\PermissionController');
    Route::resource('roles', 'Api\RoleController');

});

Route::apiResource('/marketing', 'Api\MarketingController');
Route::apiResource('/marketing-photo', 'Api\MarketingPhotoController');
Route::apiResource('/product', 'Api\ProductController');
Route::apiResource('/material-master', 'Api\MarketingInventoryController');
//CustomUpdate
Route::patch('/material-master-update','Api\MarketingInventoryController@updateMaterial');

Route::apiResource('/material-master-data', 'Api\MasterDataController');

Route::apiResource('/material-master-pagekuha', 'Api\MarketingInventoryControllerPage');
Route::get('/material-master-pagekuha-invoice', 'Api\MarketingInventoryControllerPage@getInvoice');
Route::apiResource('/material-pagekuha', 'Api\MarketingPageController');

Route::apiResource('/invoice', 'Api\InvoiceController');
Route::get('showRedeemedInvoices', 'Api\InvoiceController@showRedeemedInvoices');
Route::get('manualExport', 'Api\InvoiceController@manualExport');
Route::get('/invoice-sub/{id}', 'Api\InvoiceController@InvoiceSub');

Route::apiResource('/invoicePage', 'Api\InvoiceControllerPage');
Route::get('/invoiceSearch', 'Api\InvoiceControllerPage@InvoiceSearch');

Route::get('/findMateringInv', 'Api\MarketingInventoryControllerPage@searchMarketing');
Route::get('/findInvoiceInventory', 'Api\MarketingInventoryControllerPage@searchMarketingInv');
Route::get('/findMatering', 'Api\MarketingPageController@searchMarketing');

Route::get('/material-master-kuha', 'Api\MarketingInventoryController@master');

Route::get('/material-plant', 'Api\MarketingInventoryController@getPlant');

Route::get('/material-master-total', 'Api\MarketingInventoryController@masterTotal');


//Plant
Route::get('/plant-branches-inv', 'Api\PlantbranchController@getPlantBranch');
Route::apiResource('/plant-branches', 'Api\PlantbranchController');
Route::apiResource('plants', 'PlantController');

//MaterialMaster

Route::apiResource('/material', 'Api\MaterialMasterController');

//ImportExcel
Route::post('/import-excel', 'Api\ImportExcelController@importExcel');
Route::post('/check-import-excel', 'Api\ImportExcelController@checkImportExcel');

//SearchReport
 Route::Post('/search-report-marketing', 'Api\MarketingReportController@SearchOrderDate');
 Route::Post('/search-report-invoice', 'Api\MarketingReportController@SearchOrderDateInvoice');

 //CheckExcel
Route::get('/export-excel', 'Api\ExportExcelController@exportExcel');
//Invoice Report
Route::get('/invoice-search-report', 'Api\MarketingReportController@InvoiceReport');
Route::get('/invoice-page', 'Api\MarketingReportController@InvoicePage');

//XML EXPORT

Route::get('xml-export','Api\XMLExportController@xmlExport');

//PDF EXPORT
Route::get('pdf-export','Api\PDFExportController@pdfExport');

//XML Export
Route::apiResource('xml-cost-gl','Api\XMLMaintainanceController');

//Master Data
Route::get('master-data-invoice','Api\MasterDataInvoiceController@getInvoice');
Route::get('master-data-invoice-search','Api\MasterDataInvoiceController@getSearchInvoice');

//Inventory Per Branch
Route::get('export-excel-inv','Api\InventoryReportPerBranchController@exportExcelReport');
//Marketing Click Image
Route::get('click-image','Api\MarketingController@clickImage');
//     Route::get('/resume-product', 'Api\ProductController@resume');

//     Route::Get('/hold-product', 'Api\ProductController@allProductHold');
//     Route::Get('/ref-hold-product/{id}', 'Api\ProductController@allProductHoldRef');

//     Route::get('/productRes','ProductController@search')->name('product');
//     Route::get('/customers','CustomerController@search')->name('customer');
//     Route::get('/salesmen','SalesmanController@search')->name('salesmen');
//     Route::get('/searchPayments','PaymentOptionController@search')->name('searchPayments');
// //    Route::get('/reservationOptions','ReservationOptionController@search')->name('reservationOption');

//     //Reservation
//     Route::post('/reservation', 'ReservationController@store')->name('reservation');
//     //Master Data
//     Route::resource('payments','PaymentOptionController');
//     Route::resource('reasons','ReasonTypeController');

//     Route::resource('reservations','ReservationOptionController');


// //get product to pos controller
// Route::Get('/getting/product/{id}', 'Api\PosController@GetProduct');

// // Add to cart Route
// Route::Get('/addToCart/{id}/{qty}', 'Api\CartController@AddToCart');
// Route::Get('/addToCartResume/{id}', 'Api\CartController@AddToCartResume');
// Route::Get('/cart/product', 'Api\CartController@CartProduct');

// Route::Get('/remove/cart/{id}', 'Api\CartController@removeCart');

// Route::Get('/increment/{id}', 'Api\CartController@increment');
// Route::Get('/decrement/{id}', 'Api\CartController@decrement');

// Route::Get('/incrementAdd/{id}/{qty}', 'Api\CartController@incrementAdd');

// Route::Get('/discountCart/{id}/{disc}', 'Api\CartController@discountCart');

// Route::Get('/cart-price-add/{id}/{price}', 'Api\CartController@cartPriceAdd');
// // Route::Get('/incrementAdd/{qty}', 'Api\CartController@incrementAdd');

// // Vat Route
// Route::Get('/vats', 'Api\CartController@Vats');

// Route::Post('/orderdone', 'Api\PosController@OrderDone');
// Route::Post('/orderhold', 'Api\PosController@OrderHold');

// // Order Route
// Route::Get('/orders', 'Api\OrderController@TodayOrder');

// Route::Get('/order/details/{id}', 'Api\OrderController@OrderDetails');
// Route::Get('/order/orderdetails/{id}', 'Api\OrderController@OrderDetailsAll');

// Route::Post('/search/order', 'Api\PosController@SearchOrderDate');

// // Admin Dashboard Route

// Route::Get('/today/sell', 'Api\PosController@TodaySell');
// Route::Get('/today/income', 'Api\PosController@TodayIncome');
// Route::Get('/today/due', 'Api\PosController@TodayDue');
// Route::Get('/today/expense', 'Api\PosController@TodayExpense');
// Route::Get('/today/stockout', 'Api\PosController@Stockout');

// //Print

// Route::Get('/print-pos-transac', 'Api\PrintPosController@printPos');
